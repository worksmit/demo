(function(){
    
  'use strict';

    angular.module('app').factory('requestService',requestService);

    requestService.$inject = ['$http','$state','$q'];

    function requestService($http,$state,$q,requestService) {


        function getRequest(url){
        
            var token = window.localStorage['token'];
  

            var getResponse = $http({
              url: url,
              method: "GET",
              headers:{'Authorization' : 'Bearer '+token}
            }).success(function(data, status, headers, config){

            })
            .error(function(data, status, headers, config){

                    if(data.error == 'token_expired' || data.error == 'token_invalid' || data.error == 'token_not_provided')
                    {
                        window.localStorage.clear();
                        $state.go('login');
                    }
            });

            return getResponse;
 
        }

        function postRequest(url,data,method){
        
            var token = window.localStorage['token'];

            data = data + "&_method=" + method;

            //console.log(data);

            var postResponse = $http({
              url: url,
              method: "POST",
              data:data,
              headers:{'Authorization' : 'Bearer '+token,'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config){

            })
            .error(function(data, status, headers, config){

                    if(data.error == 'token_expired' || data.error == 'token_invalid' || data.error == 'token_not_provided')
                    {
                        window.localStorage.clear();
                        $state.go('login');
                    }
            });

            return postResponse;
 
        }


        return{
            getRequest  : getRequest,
            postRequest : postRequest
        };
    }
    
})();
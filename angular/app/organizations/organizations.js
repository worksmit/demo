(function(){

    'use strict';

    var organizations = function(datacontext,$scope,$parse,$uibModal,$uibModalStack){

        var vm = this;

        vm.head =  'Organization List';
        vm.organizationForm = {};
        $scope.organizations = [];
        $scope.states = [];
        $scope.countries = [];
        $scope.organizationForm = {};
        $scope.editOrganization = false;
        $scope.editIndex = {};
        $scope.selection = [];
        $scope.answers = [];
        $scope.submitted = false;
        $scope.totalPages = 0;
        $scope.currentPage = 1;
        $scope.range = [];

        function init(){
                   
            datacontext.state.getAll(null,'all').success(function(response) {
                    if(response.status == 'success')
                    {
                        $scope.states = response.states.data;

                    }
            });


            datacontext.country.getAll(null,'all').success(function(response){
                    if(response.status == 'success')
                    {

                        $scope.countries = response.countries.data;

                    }
            });
        }

        init();

        $scope.getListing = function(pageNumber){

            $scope.organizations = [];
            if(pageNumber===undefined){
                pageNumber = '1';
            }

            datacontext.organization.getAll(pageNumber).success(function(response){

                 if(response.status == 'success')
                    {
                        $scope.currentPage = response.organizations.current_page;
                        $scope.totalPages = response.organizations.last_page;
                        // Pagination Range
                        var pages = [];

                        for(var i=1;i<=response.organizations.last_page;i++) {          
                            pages.push(i);
                        }

                        $scope.range = pages;

                        $scope.organizations = response.organizations.data;
                    }
            });
        }

        $scope.addNewOrg = function(){

            vm.organizationForm = {};
            
            $uibModal.open({
                templateUrl: 'addOrgModal.html',
                size: 'lg',
                controller: 'OrgModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }


        $scope.edit = function(index,size){

            $scope.organizationForm = angular.copy($scope.organizations[index]);
            $scope.editIndex = index;
            $uibModal.open({
                animation: true,
                templateUrl: 'editOrgModal.html',
                size: 'lg',
                controller: 'OrgModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }


        $scope.delete = function(index){

            if(confirm('Are you sure that you want to permanently delete this record?'))
            {
                datacontext.organization.deleteOrganization($scope.organizations[index].id).success(function(result){
                    if(result.status == 'success')
                    { 
                        $scope.organizations.splice(index,1);
                    }
                });
         
            }

        }


        $scope.stateChanged = function (index,id) {

            if($scope.answers[id]){ //If it is checked
               $scope.selection.push(id);
            }
            else
            {
                var idx = $scope.selection.indexOf(id);
                $scope.selection.splice(idx, 1);
            }
        }

        $scope.deleteSelected = function(){

            if(confirm('Are you sure that you want to permanently delete selected records?'))
            {
                datacontext.organization.deleteSelected($scope.selection).success(function(result){
                    if(result.status == 'success')
                    { 
                        for (var j = $scope.selection.length-1; j >= 0 ; j--) {
                            angular.forEach($scope.organizations, function(value, key) {
                                
                                if(value.id == $scope.selection[j])
                                {
                                    $scope.organizations.splice(key,1);
                                }

                            });
                           $scope.answers[$scope.selection[j]] = false;
                            $scope.stateChanged('',$scope.selection[j]);

                        }

      /*                  vm.states = [];
                        init();*/

                    }
                });
         
            }
        }

    };

    organizations.$inject = ['datacontext','$scope','$parse','$uibModal','$uibModalStack'];

    angular
        .module('app.organizations')
            .controller('Organizations',organizations);

}());
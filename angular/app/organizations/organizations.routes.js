(function(){
    'use strict';

    var routes = function($stateProvider,$urlRouterProvider){
        
        $stateProvider
           .state('organizations',{
                url : '/organizations',
                abstract : true,
                template: '<ui-view />'
            })

            .state('organizations.list',{
                url : '/list',
                templateUrl : 'app/organizations/organizations.html',
                controller :  'Organizations as vm',
                cache : false
            })
    };

    routes.$inject = ['$stateProvider','$urlRouterProvider'];

    angular
        .module('app.organizations')
        .config(routes);

}());
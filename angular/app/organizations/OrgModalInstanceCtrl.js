(function(){

    'use strict';

    var OrgModalInstanceCtrl = function(datacontext, $scope, $parse, $uibModalInstance){

        var vm = this;

         vm.organizationForm = {};

        $scope.update = function(organizationForm){

/*
            vm.organizationForm['gm_id'] = 1;*/

            delete organizationForm['gm_id'];
            delete organizationForm['state'];

            datacontext.organization.editOrganization(organizationForm).success(function(result){
                if(result.status == 'success')
                { 
                    $scope.organizations[$scope.editIndex] = angular.copy(organizationForm);
                    angular.forEach($scope.states,function(value1, key1) {
                        if($scope.states[key1].id == organizationForm.state_id)
                        {
                           $scope.organizations[$scope.editIndex]['state'] = $scope.states[key1];
                        }
                    });
             
          
                    $scope.editOrganization = false;
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){
                    var serverMessage = $parse('organizationFormEdit.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });
          
        }


        $scope.send = function(organizationForm){


            angular.forEach($scope.states, function(value, key) {
                
                if($scope.states[key].id == organizationForm.state_id)
                {
                    organizationForm['country_id'] = $scope.states[key].country_id;
                }
            });


            datacontext.organization.saveOrganization(organizationForm).success(function(result){
                if(result.status == 'success')
                {
                    organizationForm['id'] = result.id;
                    $scope.organizations.push(organizationForm);

                    angular.forEach($scope.states,function(value1, key1) {
                    if($scope.states[key1].id == organizationForm.state_id)
                    {
                       $scope.organizations[$scope.organizations.length-1]['state'] = $scope.states[key1];
                    }
                    });
             
                    vm.organizationForm = {};
             
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){
                    var serverMessage = $parse('organizationForm.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
            
                });
                }
            });     
        }


      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
 
    };

    OrgModalInstanceCtrl.$inject = ['datacontext','$scope','$parse','$uibModalInstance'];

    angular
        .module('app.organizations')
            .controller('OrgModalInstanceCtrl',OrgModalInstanceCtrl);

}());
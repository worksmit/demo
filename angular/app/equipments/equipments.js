(function(){

    'use strict';

    var equipments = function(datacontext,$scope,$parse,$uibModal,$uibModalStack){
    
        var vm = this;
        vm.head =  'Equipments';
        vm.equipmentForm = {};
        $scope.equipmentForm = {};
        $scope.equipments = [];     
        $scope.editIndex = {};
        $scope.selection = [];
        $scope.answers = [];
        $scope.totalPages = 0;
        $scope.currentPage = 1;
        $scope.range = [];


        $scope.getListing = function(pageNumber){

            $scope.equipments = [];
            if(pageNumber===undefined){
                pageNumber = '1';
            }

            datacontext.equipments.getAll(pageNumber).success(function(response){

                 if(response.status == 'success')
                    {
                        $scope.currentPage = response.data.current_page;
                        $scope.totalPages = response.data.last_page;
                        // Pagination Range
                        var pages = [];

                        for(var i=1;i<=response.data.last_page;i++) {          
                            pages.push(i);
                        }

                        $scope.range = pages; 

                        $scope.equipments = response.data.data;
                    }
            });
        }


        $scope.addNewEquipment = function(){

            vm.equipmentForm = {};
            
            $uibModal.open({
                templateUrl: 'addEquipmentModal.html',
                size: 'lg',
                controller: 'EquipmentModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }


        $scope.edit = function(index,size){

            $scope.equipmentForm = angular.copy($scope.equipments[index]);
            $scope.editIndex = index;
            $uibModal.open({
                animation: true,
                templateUrl: 'editEquipmentModal.html',
                size: 'lg',
                controller: 'EquipmentModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }


        $scope.delete = function(index){

           

            if(confirm('Are you sure that you want to permanently delete this record?'))
            {
                datacontext.equipments.deleteEquipment($scope.equipments[index].id).success(function(result){
                    if(result.status == 'success')
                    { 
                        $scope.equipments.splice(index,1);
                    }
                });
         
            }

        }

        $scope.stateChanged = function (index,id) {

            if($scope.answers[id]){ //If it is checked
               $scope.selection.push(id);
            }
            else
            {
                var idx = $scope.selection.indexOf(id);
                $scope.selection.splice(idx, 1);
            }
        }

        $scope.deleteSelected = function(){

            if(confirm('Are you sure that you want to permanently delete selected records?'))
            {
                datacontext.equipments.deleteSelected($scope.selection).success(function(result){
                    if(result.status == 'success')
                    { 
                        for (var j = $scope.selection.length-1; j >= 0 ; j--) {
                            angular.forEach($scope.equipments, function(value, key) {
                                if(value.id == $scope.selection[j])
                                {
                                    
                                    $scope.equipments.splice(key,1);
                                }
                            });
                            $scope.answers[$scope.selection[j]] = false;
                            $scope.stateChanged('',$scope.selection[j]);

                        }
                    }
                });
         
            }
        }
    };

    equipments.$inject = ['datacontext','$scope','$parse','$uibModal','$uibModalStack'];

    angular
        .module('app.equipments')
            .controller('Equipments',equipments);

}());
(function(){

    'use strict';

    var EquipmentModalInstanceCtrl = function(datacontext, $scope, $parse, $uibModalInstance){

        var vm = this;

         vm.equipmentForm = {};

        $scope.update = function(equipmentForm){

            datacontext.equipments.editEquipment(equipmentForm).success(function(result){
                if(result.status == 'success')
                { 
                    $scope.equipments[$scope.editIndex] = angular.copy(equipmentForm);
                    $scope.submitted = false;
                    $scope.editEquipment = false;
                    $uibModalInstance.close();


                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('equipmentFormEdit.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });
          
        }

        $scope.send = function(equipmentForm){
            datacontext.equipments.saveEquipment(equipmentForm).success(function(result){
     
                if(result.status == 'success')
                {
                    equipmentForm['id'] = result.id;
                    $scope.equipments.push(equipmentForm);
                    vm.equipmentForm = {};
                    $scope.submitted = false;
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('equipmentForm.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });  
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
 
    };

    EquipmentModalInstanceCtrl.$inject = ['datacontext','$scope','$parse','$uibModalInstance'];

    angular
        .module('app.equipments')
            .controller('EquipmentModalInstanceCtrl',EquipmentModalInstanceCtrl);

}());
(function(){
    'use strict';

    var routes = function($stateProvider,$urlRouterProvider){
        
        $stateProvider
           .state('equipments',{
                url : '/equipments',
                abstract : true,
                template: '<ui-view />'
            })

            .state('equipments.list',{
                url : '/list',
                templateUrl : 'app/equipments/equipments.html',
                controller :  'Equipments as vm',
                cache : false
            })
    };

    routes.$inject = ['$stateProvider','$urlRouterProvider'];

    angular
        .module('app.equipments')
        .config(routes);

}());
(function() {
    'use strict';

    var app = angular.module('app',[
        
        'app.core',
        'app.widgets',

        /*
        * Feature Modules / areas
         */
        'app.login',
        'app.dashboard',
        'app.organizations',
        'app.schools',
        'app.colleges',
        'app.countries',
        'app.states',
        'app.bodyparts',
        'app.categories',
        'app.subcategories',
        'app.equipments',
        'app.prophylactics',
        'app.skilllevels',
        'app.specificskills',
        'app.traininglocations',
        'app.exercises',
        'app.workouts'
    ]);

    app.run(function($rootScope,$location,$state) {

         
         $rootScope.baseurl = '#';
         $rootScope.serverPath = '#';

         $rootScope.location = $location;

    });

    app.directive('postsPagination', function(){  
       return{
          restrict: 'E',
          template: '<ul class="pagination">'+
            '<li ng-show="currentPage != 1"><a href="javascript:void(0)" ng-click="getListing(1)">&laquo;</a></li>'+
            '<li ng-show="currentPage != 1"><a href="javascript:void(0)" ng-click="getListing(currentPage-1)">&lsaquo; Prev</a></li>'+
            '<li ng-repeat="i in range" ng-class="{active : currentPage == i}">'+
                '<a href="javascript:void(0)" ng-click="getListing(i)">{{i}}</a>'+
            '</li>'+
            '<li ng-show="currentPage != totalPages"><a href="javascript:void(0)" ng-click="getListing(currentPage+1)">Next &rsaquo;</a></li>'+
            '<li ng-show="currentPage != totalPages"><a href="javascript:void(0)" ng-click="getListing(totalPages)">&raquo;</a></li>'+
          '</ul>'
       };
    });

}());


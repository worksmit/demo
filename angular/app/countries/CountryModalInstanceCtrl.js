(function(){

    'use strict';

    var CountryModalInstanceCtrl = function(datacontext, $scope, $parse, $uibModalInstance){

        var vm = this;

         vm.countryForm = {};

        $scope.update = function(countryForm){

            datacontext.country.editCountry(countryForm).success(function(result){
                if(result.status == 'success')
                { 
                    $scope.countries[$scope.editIndex] = angular.copy(countryForm);
                    $scope.submitted = false;
                    $scope.editCountry = false;
                    $uibModalInstance.close();

                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('countryFormEdit.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });
          
        }

        $scope.send = function(countryForm){

            datacontext.country.saveCountry(countryForm).success(function(result){

                if(result.status == 'success')
                {
                    countryForm['id'] = result.id;
                    $scope.countries.push(countryForm);
                    vm.countryForm = {};
                    $scope.submitted = false;
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('countryForm.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });  
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
 
    };

    CountryModalInstanceCtrl.$inject = ['datacontext','$scope','$parse','$uibModalInstance'];

    angular
        .module('app.countries')
            .controller('CountryModalInstanceCtrl',CountryModalInstanceCtrl);

}());
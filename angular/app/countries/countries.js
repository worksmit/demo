(function(){

    'use strict';

    var countries = function(datacontext,$scope,$parse,$uibModal,$uibModalStack){

        var vm = this;

        vm.head =  'Country List';
        $scope.countries = [];
        vm.countryForm = {};
        $scope.countryForm = {};
        $scope.editCountry = false;
        $scope.editIndex = {};
        $scope.selection = [];
        $scope.answers = [];
        $scope.submitted = false;
        $scope.totalPages = 0;
        $scope.currentPage = 1;
        $scope.range = [];

        $scope.getListing = function(pageNumber){

            $scope.countries = [];
            if(pageNumber===undefined){
                pageNumber = '1';
            }

            datacontext.country.getAll(pageNumber).success(function(response){

                 if(response.status == 'success')
                    {
                        $scope.currentPage = response.countries.current_page;
                        $scope.totalPages = response.countries.last_page;
                        // Pagination Range
                        var pages = [];

                        for(var i=1;i<=response.countries.last_page;i++) {          
                            pages.push(i);
                        }

                        $scope.range = pages; 
                   
                        $scope.countries = response.countries.data;

                    }
            });
        }

        $scope.addNewCountry = function(){

            vm.countryForm = {};
            
            $uibModal.open({
                templateUrl: 'addCountryModal.html',
                size: 'lg',
                controller: 'CountryModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }

        $scope.edit = function(index,size){

            $scope.countryForm = angular.copy($scope.countries[index]);
            $scope.editIndex = index;
            $uibModal.open({
                animation: true,
                templateUrl: 'editCountryModal.html',
                size: 'lg',
                controller: 'CountryModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }
        
        $scope.delete = function(index){


            if(confirm('Are you sure that you want to permanently delete this record?'))
            {
                datacontext.country.deleteCountry($scope.countries[index].id).success(function(result){
                    if(result.status == 'success')
                    { 
                        $scope.countries.splice(index,1);
                    }
                });
         
            }

        }

        $scope.stateChanged = function (index,id) {

            if($scope.answers[id]){ //If it is checked
               $scope.selection.push(id);
            }
            else
            {
                var idx = $scope.selection.indexOf(id);
                $scope.selection.splice(idx, 1);
            }
        }

        $scope.deleteSelected = function(){

            if(confirm('Are you sure that you want to permanently delete selected records?'))
            {
                datacontext.country.deleteSelected($scope.selection).success(function(result){
                    if(result.status == 'success')
                    { 
                        for (var j = $scope.selection.length-1; j >= 0 ; j--) {
                            angular.forEach($scope.countries, function(value, key) {
                                
                                if(value.id == $scope.selection[j])
                                {
                                    $scope.countries.splice(key,1);
                                }

                            });
                            $scope.answers[$scope.selection[j]] = false;
                            $scope.stateChanged('',$scope.selection[j]);

                        }

      /*                  vm.states = [];
                        init();*/

                    }
                });
         
            }
        }
    };

    countries.$inject = ['datacontext','$scope','$parse','$uibModal','$uibModalStack'];

    angular
        .module('app.countries')
            .controller('Countries',countries);

}());
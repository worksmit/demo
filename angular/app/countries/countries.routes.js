(function(){
    'use strict';

    var routes = function($stateProvider,$urlRouterProvider){
        
        $stateProvider
           .state('countries',{
                url : '/countries',
                abstract : true,
                template: '<ui-view />'
            })

            .state('countries.list',{
                url : '/list',
                templateUrl : 'app/countries/countries.html',
                controller :  'Countries as vm',
                cache : false
            })
    };

    routes.$inject = ['$stateProvider','$urlRouterProvider'];

    angular
        .module('app.countries')
        .config(routes);

}());
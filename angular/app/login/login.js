(function(){

    'use strict';

    var login = function(datacontext,$scope,$rootScope,$http,$state,$parse){
    

		$scope.submitted = false;


        if(window.localStorage['token'])
        {
            $state.go('dashboard');
        }

		$scope.send = function(loginData){


			$http({
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  url: $rootScope.baseurl+'login',
                  method: "POST",
                  data: $.param(loginData),
                }).success(function(response){


                	if(response.status == 'fail')
                	{
                		var serverMessage = $parse('loginForm.serverError.$error.serverMessage');
                        serverMessage.assign($scope, response.message);
                	}
                	else
                	{
	                	window.localStorage['token'] = response.token;
	                	$state.go('dashboard');
                	}
                });
		}
    };

    login.$inject = ['datacontext','$scope','$rootScope','$http','$state','$parse'];

    angular
        .module('app.login')
            .controller('Login',login);

}());
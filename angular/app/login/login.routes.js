(function(){
    'use strict';

    var routes = function($stateProvider,$urlRouterProvider){
        
        $stateProvider
         .state('login',{
                url : '/auth/login',
                //abstract : true,
                templateUrl : 'app/login/login.html',
                controller :  'Login as vm'
            });
    };

    routes.$inject = ['$stateProvider','$urlRouterProvider'];

    angular
        .module('app.login')
        .config(routes);

}());
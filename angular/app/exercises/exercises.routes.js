(function(){
    'use strict';

    var routes = function($stateProvider,$urlRouterProvider){
        
        $stateProvider
           .state('exercises',{
                url : '/exercises',
                abstract : true,
                template: '<ui-view />'
            })

    /*        .state('exercises.list',{
                url : '/list',
                templateUrl : 'app/exercises/exercises.html',
                controller :  'Exercises as vm',
                cache : false
            })*/

            .state('exercises.list',{
                url : '/list',
                templateUrl : 'app/exercises/exercise-new.html',
                controller :  'Exercises as vm',
                cache : false
            })

      /*      .state('exercises.edit',{
                url : '/edit?id={exerciseId}',
                templateUrl : 'app/exercises/edit-exercises.html',
                controller :  'EditExercises as vm',
                cache : false
            })*/

            .state('exercises.edit',{
                url : '/edit?id={exerciseId}',
                templateUrl : 'app/exercises/edit-exercise-new.html',
                controller :  'EditExercises as vm',
                cache : false
            })

  /*          .state('exercises.add',{
                url : '/add',
                templateUrl : 'app/exercises/add-exercises.html',
                controller :  'Exercises as vm',
                cache : false
            })*/

            .state('exercises.add',{
                url : '/add',
                templateUrl : 'app/exercises/add-exercise-new.html',
                controller :  'Exercises as vm',
                cache : false
            })
    };

    routes.$inject = ['$stateProvider','$urlRouterProvider'];

    angular
        .module('app.exercises')
        .config(routes);

}());
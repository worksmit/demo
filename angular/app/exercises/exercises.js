(function(){

    'use strict';

    var exercises = function(datacontext,$scope,$parse,$uibModal,$uibModalStack,$state,$stateParams,$timeout,$rootScope,Upload){
    
        var vm = this;
        vm.head =  'Exercises';
        $scope.traininglocations = [];
        $scope.categories = [];
        $scope.subcategories = [];
        $scope.skilllevels = [];
        $scope.specificskills = [];
        $scope.bodyparts = [];
        $scope.equipments = [];
        $scope.exercises = [];
        $scope.exerciseFormFieldEdit = {};
        $scope.picture = {};
        $scope.totalPages = 0;
        $scope.currentPage = 1;
        $scope.range = [];
        $scope.exerciseFormField = {};
        $scope.fileLoading = false;
        $scope.upload1 = 0;
        $scope.upload2 = 0;
        $scope.plus1 = 1;
        $scope.plus2 = 0;
        $scope.upload3 = 0;
        $scope.upload4 = 0;
        $scope.plus3 = 1;
        $scope.plus4 = 0;


        function init(){

               datacontext.exercises.getAllMasters().success(function(response) {

                    if(response.status == 'success')
                    {
                        $scope.categories = response.masters.categories;
                        $scope.bodyparts = response.masters.body_parts;
                        $scope.equipments = response.masters.equipments;
                        $scope.skilllevels = response.masters.skilllevels;
                        $scope.specificskills = response.masters.specific_skills;
                        $scope.subcategories = response.masters.sub_categories;
                        $scope.traininglocations = response.masters.training_locations;
                    }
            });
        }

        init();

        $scope.showUpload = function(index){

            console.log(index);
            if(index == 1){

                $scope.upload1 = 1;
                $scope.plus1 = 0;
                $scope.plus2 = 1;
            }else if(index == 2){

                $scope.upload2 = 1;
                $scope.plus2 = 0;
            }else if(index == 3){

                $scope.upload3 = 1;
                $scope.plus3 = 0;
                $scope.plus4 = 1;
            }else if(index == 4){

                $scope.upload4 = 1;
                $scope.plus4 = 0;
            }
        }



        $scope.exerciseFormField.exercise_assets = [];
       $scope.uploadFiles = function(file, errFiles , ind ,type) {

            console.log(type);
            var a = file.name.split('.');
            console.log(a[1]);
            var b = a[1].toLowerCase();
            if(b == 'gif' && type != 'gif' ){

                  alert("Please select gif file");
                  return false;
            }
            console.log((b == 'jpg' && type != 'image') || (b == 'png' && type != 'image') || (b == 'jpeg' && type != 'image'))
            if((b == 'jpg' && type != 'image') || (b == 'png' && type != 'image') || (b == 'jpeg' && type != 'image')){

                alert("Please select jpeg,png or jpg file");
                return false;
            }

            if(b == 'mp4' && type != 'video' ){

                alert("Please select mp4 video");
                return false;
            }
           /* var ext = evt1.currentTarget.files[0].name.match(/\.(.+)$/)[1];

            if(angular.lowercase(ext) ==='jpg' || angular.lowercase(ext) ==='jpeg' || angular.lowercase(ext) ==='png'){

            }
            else{
                UIkit.modal.alert("Invalid image format, only jpg and png format allowed");
                // alert("Invalid File Format");
                return -1;
            }*/

              //console.log(ind);

              $scope.fileLoading = true;
              $scope.f = file;
              $scope.errFile = errFiles && errFiles[0];
              if (file) {
                  file.upload = Upload.upload({
                      url:  $rootScope.baseurl + 'exercises/upload_temp_asset?token=' + window.localStorage['token'],
                      data: {'exercise_asset[]': file}
                      
                  });

                  file.upload.then(function (response) {

                    if(response.data.status == 'success'){
                        $scope.fileLoading = false;
                        
                        
                        var res_file = response.data.data.asset_name;
                        $scope.exerciseFormField.exercise_assets[ind] = res_file;
                        console.log($scope.exerciseFormField.exercise_assets);
                    }
                      $timeout(function () {
                          file.result = response.data;
                      });
                  }, function (response) {
                      if (response.status > 0)
                          $scope.errorMsg = response.status + ': ' + response.data;
                  }, function (evt) {
                      file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                  });
              }   
        }



        $scope.getListing = function(pageNumber){

            $scope.exercises = [];
            if(pageNumber===undefined){
                pageNumber = '1';
            }

            datacontext.exercises.getAll(pageNumber).success(function(response){

                 if(response.status == 'success')
                    {
                        $scope.currentPage = response.exercises.current_page;
                        $scope.totalPages = response.exercises.last_page;
                        // Pagination Range
                        var pages = [];

                        for(var i=1;i<=response.exercises.last_page;i++) {          
                            pages.push(i);
                        }

                        $scope.range = pages; 

                        $scope.exercises = response.exercises.data;

                    }
            });
        }

        $scope.remove_files = function(ind){

            console.log(ind);
            var f1 = $scope.exerciseFormField.exercise_assets[ind];
            var index = $scope.exerciseFormField.exercise_assets.indexOf(f1);
            if (index > -1) {
                $scope.exerciseFormField.exercise_assets.splice(index, 1);
            }
            
            if(ind == 1){
                $scope.picture.picFile1 = null;
            }else if(ind == 2){

                $scope.picture.picFile2 = null;
            }else if(ind == 3){

                $scope.picture.picFile3 = null;
            }else if(ind == 4){

                $scope.picture.vid1 = null;
            }else if(ind == 5){

                $scope.picture.gif1 = null;
            }


            datacontext.exercises.removeFiles(f1).success(function(response) {
                console.log(response);
            /*        if(response.status == 'success')
                    {
                         $scope.equipments = response.data.data;

                    }*/
            });

        }


        $scope.add = function(){

            $state.go('exercises.add');
        }

        $scope.edit = function(index){
            
            $scope.exerciseFormFieldEdit = angular.copy($scope.exercises[index]);

            $state.go('exercises.edit',{'exerciseId':$scope.exercises[index].id},{ reload: true });
        }

        $scope.delete = function(index){

            if(confirm('Are you sure that you want to permanently delete this exercise?'))
            {

                datacontext.exercises.deleteExercise($scope.exercises[index].id).success(function(result){
                    if(result.status == 'success')
                    { 
                        $scope.exercises.splice(index,1);
                    }
                });
            }
        }

        $scope.send = function(){

            datacontext.exercises.saveExercise($scope.exerciseFormField).success(function(response) {

                    if(response.status == 'success')
                    {

                        $state.go('exercises.list');

                    }
            });
        }

    // for multiple files:
 /*   $scope.uploadExerciseVideos = function (files) {
       
      if (files && files.length) {
        angular.forEach(files,function(value,key){

        
            Upload.base64DataUrl(value).then(function(urls){


                $scope.exerciseFormField.exercise_assets_video[key] = urls;
            });
        });

      }

    }*/


     // for multiple files:
    /*$scope.uploadExerciseImages = function (files) {
        
      if (files && files.length) {
    
        angular.forEach(files,function(value,key){

            Upload.base64DataUrl(value).then(function(urls){

            $scope.exerciseFormField.exercise_assets_image[key] = urls;
        });
        });
    
      }
    }*/

     // for multiple files:
/*    $scope.uploadExerciseGifs = function (files) {
       
      if (files && files.length) {

        angular.forEach(files,function(value,key){

            Upload.base64DataUrl(value).then(function(urls){


            $scope.exerciseFormField.exercise_assets_gif[key] = urls;
        });
        });
  
      }

    }*/

    };

    exercises.$inject = ['datacontext','$scope','$parse','$uibModal','$uibModalStack','$state','$stateParams','$timeout','$rootScope','Upload'];

    angular
        .module('app.exercises')
            .controller('Exercises',exercises);

}());
(function(){

    'use strict';

    var editexercises = function(datacontext,Upload,$timeout,$scope,$parse,$uibModal,$uibModalStack,$state,$stateParams,$rootScope){
    
        var vm = this;
        vm.head =  'Exercises';
        $scope.traininglocations = [];
        $scope.categories = [];
        $scope.subcategories = [];
        $scope.skilllevels = [];
        $scope.specificskills = [];
        $scope.bodyparts = [];
        $scope.equipments = [];
        $scope.exercises = [];
        $scope.exerciseFormFieldEdit = {};
        $scope.picture = {};
        $scope.totalPages = 0;
        $scope.currentPage = 1;
        $scope.range = [];
        $scope.upload1 = 0;
        $scope.upload2 = 0;
        $scope.plus1 = 1;
        $scope.plus2 = 0;
        $scope.p1 = 0;
        $scope.p2 = 0;
        $scope.p3 = 0;
        $scope.p4 = 0;
        $scope.p5 = 0;
        $scope.imgUrl = $rootScope.serverPath+"/images/exerciseassets/";
  
        function init(){

            datacontext.exercises.getAllMasters().success(function(response) {

                    if(response.status == 'success')
                    {
                        $scope.categories = response.masters.categories;
                        $scope.bodyparts = response.masters.body_parts;
                        $scope.equipments = response.masters.equipments;
                        $scope.skilllevels = response.masters.skilllevels;
                        $scope.specificskills = response.masters.specific_skills;
                        $scope.subcategories = response.masters.sub_categories;
                        $scope.traininglocations = response.masters.training_locations;
                    }
            });

            $scope.exerciseFormFieldEdit.exercise_assets = [];
            datacontext.exercises.getExerciseById($stateParams.exerciseId).success(function(response) {
              
                    if(response.status == 'success')
                    {
                            var i1 = 1;
                            
                            angular.forEach(response.exercise[0].assets , function(value , key){
                                
                                
                                if(value.asset_type == 'image' && i1 == 1){
                                    $scope.p1 = 1;
                                    $scope.pic1 = value.asset_url;
                                    //$scope.upload1 = 1;
                                    $scope.plus1 = 1;
                                    //$scope.plus2 = 0;
                                    i1++;
                                }else if(value.asset_type == 'image' && i1 == 2){
                                    $scope.p2 = 1;
                                    $scope.pic2 =  value.asset_url;
                                    $scope.upload1 = 1;
                                    $scope.plus1 = 0;
                                    $scope.plus2 = 1;
                                    i1++;
                                }else if(value.asset_type == 'image' && i1 == 3){
                                    $scope.p3 = 1;
                                    $scope.upload2 = 1;
                                    
                                    $scope.pic3 = value.asset_url;
                                }else if(value.asset_type == 'video'){
                                    $scope.p4 = 1;
                                    $scope.pic4 = value.asset_url;
                                }else if(value.asset_type == 'gif'){

                                    $scope.p5 = 1;
                                    $scope.pic5 = value.asset_url;
                                }
                            });
                            $scope.exerciseFormFieldEdit = angular.copy(response.exercise[0]);
                            $scope.exerciseFormFieldEdit.assets_to_be_removed = [];
                            $scope.exerciseFormFieldEdit.exercise_assets = [];
                            // $scope.exerciseFormFieldEdit.skilllevel_id = [];
                            // angular.forEach($scope.exerciseFormFieldEdit.skilllevels,function(value,key){
                            //     $scope.exerciseFormFieldEdit.skilllevel_id.push(value.id);
                            // });
                            $scope.exerciseFormFieldEdit.specificskill_id = [];
                            angular.forEach($scope.exerciseFormFieldEdit.specificskills,function(value,key){
                                $scope.exerciseFormFieldEdit.specificskill_id.push(value.id);
                            });
                            $scope.exerciseFormFieldEdit.bodypart_id = [];
                            angular.forEach($scope.exerciseFormFieldEdit.bodyparts,function(value,key){
                                $scope.exerciseFormFieldEdit.bodypart_id.push(value.id);
                            });
                            $scope.exerciseFormFieldEdit.equipment_id = [];
                            angular.forEach($scope.exerciseFormFieldEdit.equipments,function(value,key){
                                 $scope.exerciseFormFieldEdit.equipment_id.push(value.id);
                            });
                    }
            });
           
        }

        init();

        $scope.showUpload = function(index){

            if(index == 1){

                $scope.upload1 = 1;
                $scope.plus1 = 0;
                $scope.plus2 = 1;
            }else if(index == 2){

                $scope.upload2 = 1;
                $scope.plus2 = 0;
            }
        }
        
        $scope.remove_files = function(ind){


            var f1 = $scope.exerciseFormFieldEdit.exercise_assets[ind];
            var index = $scope.exerciseFormFieldEdit.exercise_assets.indexOf(f1);
            if (index > -1) {
                $scope.exerciseFormFieldEdit.exercise_assets.splice(index, 1);
            }
            if(ind == 1){
                $scope.picture.picFile1 = null;
            }else if(ind == 2){

                $scope.picture.picFile2 = null;
            }else if(ind == 3){

                $scope.picture.picFile3 = null;
            }else if(ind == 4){

                $scope.picture.vid1 = null;
            }else if(ind == 5){

                $scope.picture.gif1 = null;
            }


            datacontext.exercises.removeFiles(f1).success(function(response) {
                   /* if(response.status == 'success')
                    {
                         $scope.equipments = response.data.data;

                    }*/
            });

        }

        //$scope.exerciseFormFieldEdit1.assets_to_be_removed = [];
        $scope.remove_file_array = function(index,p){
            $scope.exerciseFormFieldEdit.assets_to_be_removed.push(index);
            if(p == 1){

                $scope.p1 = 0;
            }else if(p == 2){

                $scope.p2 = 0;
            }else if(p == 3){

                $scope.p3 = 0;
            }else if(p == 4){

                $scope.p4 = 0;
            }else if(p == 5){

                $scope.p5 = 0;
            }
            //console.log(index);
           /* var temp = '$scope.pic' + index;
            console.log(temp);
            //$scope.exerciseFormFieldEdit1.assets_to_be_removed.push(index); */
            console.log($scope.exerciseFormFieldEdit.assets_to_be_removed);
        }

        $scope.uploadFiles = function(file, errFiles , ind ,type) {
      
            if(ind == 1){

                if($scope.p1 == 1){
                    $scope.exerciseFormFieldEdit.assets_to_be_removed.push($scope.pic1);
                    $scope.p1 = 0;
                }

            }

            if(ind == 2){

                if($scope.p2 == 1){
                    $scope.exerciseFormFieldEdit.assets_to_be_removed.push($scope.pic2);
                    $scope.p2 = 0;
                }

            }

            if(ind == 3){

                if($scope.p3 == 1){
                    $scope.exerciseFormFieldEdit.assets_to_be_removed.push($scope.pic3);
                    $scope.p3 = 0;
                }

            }

            if(ind == 4){

                if($scope.p4 == 1){
                    $scope.exerciseFormFieldEdit.assets_to_be_removed.push($scope.pic4);
                    $scope.p4 = 0;
                }

            }

            if(ind == 5){

                if($scope.p5 == 1){
                    $scope.exerciseFormFieldEdit.assets_to_be_removed.push($scope.pic5);
                    $scope.p5 = 0;
                }

            }

             
             var a = file.name.split('.');
             var b = a[1].toLowerCase();
             if(b == 'gif' && type != 'gif' ){

                   alert("Please select gif file");
                   return false;
             }
             if((b == 'jpg' && type != 'image') || (b == 'png' && type != 'image') || (b == 'jpeg' && type != 'image')){

                 alert("Please select jpeg,png or jpg file");
                 return false;
             }

             if(b == 'mp4' && type != 'video' ){

                 alert("Please select mp4 video");
                 return false;
             }
            /* var ext = evt1.currentTarget.files[0].name.match(/\.(.+)$/)[1];

             if(angular.lowercase(ext) ==='jpg' || angular.lowercase(ext) ==='jpeg' || angular.lowercase(ext) ==='png'){

             }
             else{
                 UIkit.modal.alert("Invalid image format, only jpg and png format allowed");
                 // alert("Invalid File Format");
                 return -1;
             }*/

               //console.log(ind);

               $scope.fileLoading = true;
               $scope.f = file;
               $scope.errFile = errFiles && errFiles[0];
               if (file) {
                   file.upload = Upload.upload({
                       url: $rootScope.baseurl + 'exercises/upload_temp_asset?token=' + window.localStorage['token'],
                       data: {'exercise_asset[]': file}
                      
                   });

                   file.upload.then(function (response) {
                    console.log(response);
                     if(response.data.status == 'success'){
                         $scope.fileLoading = false;
                         
                         
                         var res_file = response.data.data.asset_name;


                         $scope.exerciseFormFieldEdit.exercise_assets[ind] = res_file;
                        console.log($scope.exerciseFormFieldEdit.exercise_assets);
                     }
                       $timeout(function () {
                           file.result = response.data;
                       });
                   }, function (response) {
                       if (response.status > 0)
                           $scope.errorMsg = response.status + ': ' + response.data;
                   }, function (evt) {
                       file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                   });
               }   
               
         }

        $scope.update = function(){

            datacontext.exercises.editExercise($scope.exerciseFormFieldEdit).success(function(response) {
                
                    if(response.status == 'success')
                    {

                        $state.go('exercises.list');
     
                    }
            });
        }

 

    };

    editexercises.$inject = ['datacontext','Upload','$timeout','$scope','$parse','$uibModal','$uibModalStack','$state','$stateParams','$rootScope'];

    angular
        .module('app.exercises')
            .controller('EditExercises',editexercises);

}());
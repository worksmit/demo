(function(){
    'use strict';

    var routes = function($stateProvider,$urlRouterProvider){
        
        $stateProvider
           .state('categories',{
                url : '/categories',
                abstract : true,
                template: '<ui-view />'
            })

            .state('categories.list',{
                url : '/list',
                templateUrl : 'app/categories/categories.html',
                controller :  'Categories as vm',
                cache : false
            })
    };

    routes.$inject = ['$stateProvider','$urlRouterProvider'];

    angular
        .module('app.categories')
        .config(routes);

}());
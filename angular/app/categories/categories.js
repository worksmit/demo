(function(){

    'use strict';

    var categories = function(datacontext,$scope,$parse,$uibModal,$uibModalStack){
    
        var vm = this;
        vm.head =  'Categories';
        vm.categoryForm = {};
        $scope.categoryForm = {};
        $scope.categories = [];     
        $scope.editIndex = {};
        $scope.selection = [];
        $scope.answers = [];
        $scope.totalPages = 0;
        $scope.currentPage = 1;
        $scope.range = [];


        $scope.getListing = function(pageNumber){

            $scope.categories = [];
            if(pageNumber===undefined){
                pageNumber = '1';
            }

            datacontext.categories.getAll(pageNumber).success(function(response){

                 if(response.status == 'success')
                    {
                        $scope.currentPage = response.categories.current_page;
                        $scope.totalPages = response.categories.last_page;
                        // Pagination Range
                        var pages = [];

                        for(var i=1;i<=response.categories.last_page;i++) {          
                            pages.push(i);
                        }

                        $scope.range = pages; 

                        $scope.categories = response.categories.data;
                    }
            });
        }

        $scope.addNewCategory = function(){

            vm.categoryForm = {};
            
            $uibModal.open({
                templateUrl: 'addCategoryModal.html',
                size: 'lg',
                controller: 'CategoryModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }


        $scope.edit = function(index,size){

            $scope.categoryForm = angular.copy($scope.categories[index]);
            $scope.editIndex = index;
            $uibModal.open({
                animation: true,
                templateUrl: 'editCategoryModal.html',
                size: 'lg',
                controller: 'CategoryModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }


        $scope.delete = function(index){

           

            if(confirm('Are you sure that you want to permanently delete this record?'))
            {
                datacontext.categories.deleteCategory($scope.categories[index].id).success(function(result){
                    if(result.status == 'success')
                    { 
                        $scope.categories.splice(index,1);
                    }
                });
         
            }

        }
        
        $scope.stateChanged = function (index,id) {

            if($scope.answers[id]){ //If it is checked
               $scope.selection.push(id);
            }
            else
            {
                var idx = $scope.selection.indexOf(id);
                $scope.selection.splice(idx, 1);
            }
        }

        $scope.deleteSelected = function(){

            if(confirm('Are you sure that you want to permanently delete selected records?'))
            {
                datacontext.categories.deleteSelected($scope.selection).success(function(result){
                    if(result.status == 'success')
                    { 
                        for (var j = $scope.selection.length-1; j >= 0 ; j--) {
                            angular.forEach($scope.categories, function(value, key) {
                                if(value.id == $scope.selection[j])
                                {
                                    
                                    $scope.categories.splice(key,1);
                                }
                            });
                            $scope.answers[$scope.selection[j]] = false;
                            $scope.stateChanged('',$scope.selection[j]);

                        }
                    }
                });
         
            }
        }
    };

    categories.$inject = ['datacontext','$scope','$parse','$uibModal','$uibModalStack'];

    angular
        .module('app.categories')
            .controller('Categories',categories);

}());
(function(){

    'use strict';

    var CategoryModalInstanceCtrl = function(datacontext, $scope, $parse, $uibModalInstance){

        var vm = this;

         vm.categoryForm = {};

        $scope.update = function(categoryForm){

            datacontext.categories.editCategory(categoryForm).success(function(result){
                if(result.status == 'success')
                { 
                    $scope.categories[$scope.editIndex] = angular.copy(categoryForm);
                    $scope.submitted = false;
                    $uibModalInstance.close();


                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('categoryFormEdit.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });
          
        }

        $scope.send = function(categoryForm){
            datacontext.categories.saveCategory(categoryForm).success(function(result){
     
                if(result.status == 'success')
                {
                    categoryForm['id'] = result.id;
                    $scope.categories.push(categoryForm);
                    vm.categoryForm = {};
                    $scope.submitted = false;
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('categoryForm.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });  
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
 
    };

    CategoryModalInstanceCtrl.$inject = ['datacontext','$scope','$parse','$uibModalInstance'];

    angular
        .module('app.categories')
            .controller('CategoryModalInstanceCtrl',CategoryModalInstanceCtrl);

}());
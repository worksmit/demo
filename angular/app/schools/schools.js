(function(){

    'use strict';

    var schools = function(datacontext,$scope,$parse,$uibModal,$uibModalStack){

        var vm = this;

        vm.head =  'Schools';
        vm.schoolForm = {};
        $scope.schoolForm = {};
        $scope.schools = [];
        $scope.states = [];
        $scope.countries = [];
        $scope.editSchool = false;
        $scope.editIndex = {};
        $scope.selection = [];
        $scope.answers = [];
        $scope.submitted = false;
        $scope.totalPages = 0;
        $scope.currentPage = 1;
        $scope.range = [];

        function init(){

            datacontext.state.getAll(null,'all').success(function(response) {
                    if(response.status == 'success')
                    {
 
                        $scope.states = response.states.data;

                    }
            });


            datacontext.country.getAll(null,'all').success(function(response){
                    if(response.status == 'success')
                    {
    
                         $scope.countries = response.countries.data;

                    }
            });


        }

        init();

        $scope.getListing = function(pageNumber){

            $scope.schools = [];
            if(pageNumber===undefined){
                pageNumber = '1';
            }

            datacontext.school.getAll(pageNumber).success(function(response) {
                if(response.status == 'success')
                {
        
                    $scope.currentPage = response.schools.current_page;
                    $scope.totalPages = response.schools.last_page;
                    // Pagination Range
                    var pages = [];

                    for(var i=1;i<=response.schools.last_page;i++) {          
                        pages.push(i);
                    }

                    $scope.range = pages; 

                    $scope.schools = response.schools.data;
                }
            });
   
        }

        $scope.addNewSchool = function(){

            vm.schoolForm = {};
            
            $uibModal.open({
                templateUrl: 'addSchoolModal.html',
                size: 'lg',
                controller: 'SchoolModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }


        $scope.edit = function(index,size){

            $scope.schoolForm = angular.copy($scope.schools[index]);
            $scope.editIndex = index;
            $uibModal.open({
                animation: true,
                templateUrl: 'editSchoolModal.html',
                size: 'lg',
                controller: 'SchoolModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }

        $scope.delete = function(index){

            if(confirm('Are you sure that you want to permanently delete this record?'))
            {
                datacontext.school.deleteSchool($scope.schools[index].id).success(function(result){
                    if(result.status == 'success')
                    { 
                        $scope.schools.splice(index,1);
                    }
                });
         
            }

        }


        $scope.stateChanged = function (index,id) {

            if($scope.answers[id]){ //If it is checked
               $scope.selection.push(id);
            }
            else
            {
                var idx = $scope.selection.indexOf(id);
                $scope.selection.splice(idx, 1);
            }
        }

        $scope.deleteSelected = function(){

            if(confirm('Are you sure that you want to permanently delete selected records?'))
            {
                datacontext.school.deleteSelected($scope.selection).success(function(result){
                    if(result.status == 'success')
                    { 
                        for (var j = $scope.selection.length-1; j >= 0 ; j--) {
                            angular.forEach($scope.schools, function(value, key) {
                                
                                if(value.id == $scope.selection[j])
                                {
                                    $scope.schools.splice(key,1);
                                }

                            });
                           $scope.answers[$scope.selection[j]] = false;
                            $scope.stateChanged('',$scope.selection[j]);

                        }

                    }
                });
         
            }
        }

    };

    schools.$inject = ['datacontext','$scope','$parse','$uibModal','$uibModalStack'];

    angular
        .module('app.schools')
        .controller('Schools',schools);

}());
(function(){
    'use strict';

    var routes = function($stateProvider,$urlRouterProvider){

        $stateProvider
            .state('schools',{
                url : '/schools',
                abstract : true,
                template: '<ui-view />'
            })

            .state('schools.list',{
                url : '/list',
                templateUrl : 'app/schools/schools.html',
                controller :  'Schools as vm',
                cache : false
            })
    };

    routes.$inject = ['$stateProvider','$urlRouterProvider'];

    angular
        .module('app.schools')
        .config(routes);

}());
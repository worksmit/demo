(function(){

    'use strict';

    var SchoolModalInstanceCtrl = function(datacontext, $scope, $parse, $uibModalInstance){

        var vm = this;

         vm.schoolForm = {};

        $scope.update = function(schoolForm){

            delete schoolForm['state'];

            datacontext.school.editSchool(schoolForm).success(function(result){
                if(result.status == 'success')
                { 
                    $scope.schools[$scope.editIndex] = angular.copy(schoolForm);
                    angular.forEach($scope.states,function(value1, key1) {
                    if($scope.states[key1].id == schoolForm.state_id)
                    {
                       $scope.schools[$scope.editIndex]['state'] = $scope.states[key1];
                    }
                    });
                    $scope.submitted = false;
                    $scope.editSchool = false;
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){
                    var serverMessage = $parse('schoolFormEdit.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });
          
        }

        $scope.send = function(schoolForm){

            angular.forEach($scope.states, function(value, key) {
                
                if($scope.states[key].id == schoolForm.state_id)
                {
                    schoolForm['country_id'] = $scope.states[key].country_id;
                }
            });

            datacontext.school.saveSchool(schoolForm).success(function(result){
                if(result.status == 'success')
                {
                    schoolForm['id'] = result.id;
                    $scope.schools.push(schoolForm);


                    angular.forEach($scope.states,function(value1, key1) {
                    if($scope.states[key1].id == schoolForm.state_id)
                    {
                       $scope.schools[$scope.schools.length-1]['state'] = $scope.states[key1];
                    }
                    });
                    vm.schoolForm = {};
                    $scope.submitted = false;
                    $uibModalInstance.close();
                }

                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){
                    var serverMessage = $parse('schoolForm.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });     
        }



      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
 
    };

    SchoolModalInstanceCtrl.$inject = ['datacontext','$scope','$parse','$uibModalInstance'];

    angular
        .module('app.schools')
            .controller('SchoolModalInstanceCtrl',SchoolModalInstanceCtrl);

}());
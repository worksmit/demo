(function(){

    'use strict';

    var ModalInstanceCtrl = function(datacontext, $scope, $parse, $uibModalInstance){

        var vm = this;

         vm.stateForm = {};

           $scope.update = function(stateForm){


            delete stateForm['country_name'];
            datacontext.state.editState(stateForm).success(function(result){
                if(result.status == 'success')
                { 
                    $scope.states[$scope.editIndex] = angular.copy(stateForm);
                    angular.forEach($scope.countries,function(value1, key1) {
                    if($scope.countries[key1].id == stateForm.country_id)
                    {
                       $scope.states[$scope.editIndex]['country_name'] = $scope.countries[key1];
                    }
                    });
                    angular.forEach(stateForm,function(value,key){
                        var serverMessage = $parse('stateFormEdit.'+key+'.$error.serverMessage');
                        serverMessage.assign($scope, undefined);
                    });
                    
                    $scope.submitted = false;
                    $scope.editState = false;
                     $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('stateFormEdit.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
            
                });
                }
            });
          
        }


        $scope.send = function(stateForm){


            datacontext.state.saveState(stateForm).success(function(result){
                if(result.status == 'success')
                {
                    stateForm['id'] = result.id;
                    $scope.states.push(stateForm);
                    angular.forEach($scope.countries,function(value1, key1) {
                    if($scope.countries[key1].id == stateForm.country_id)
                    {
                       $scope.states[$scope.states.length-1]['country_name'] = {};
                       $scope.states[$scope.states.length-1]['country_name']['name'] = $scope.countries[key1].name;
                    }
                    });
                    angular.forEach(stateForm,function(value,key){
                        var serverMessage = $parse('stateForm.'+key+'.$error.serverMessage');
                        serverMessage.assign($scope, undefined);
                    });
                    vm.stateForm = {};
                    $scope.submitted = false;
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('stateForm.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
            
                });
                }
            });     
        }


      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
 
    };

    ModalInstanceCtrl.$inject = ['datacontext','$scope','$parse','$uibModalInstance'];

    angular
        .module('app.states')
            .controller('ModalInstanceCtrl',ModalInstanceCtrl);

}());
(function(){

    'use strict';

    var states = function(datacontext,$scope,$parse,$uibModal,$uibModalStack){

        var vm = this;

        vm.head =  'State List';
        vm.stateForm = {};
        $scope.states = [];
        $scope.countries = [];
        $scope.stateForm = {};
        $scope.editState = false;
        $scope.editIndex = {};
        $scope.selection = [];
        $scope.answers = [];
        $scope.submitted = false;
        $scope.totalPages = 0;
        $scope.currentPage = 1;
        $scope.range = [];
    

        function init(){
            
 
            datacontext.country.getAll(null,'all').success(function(response){
                    if(response.status == 'success')
                    {
                        $scope.countries = response.countries.data;
                    }
            });
        }

        init();


        $scope.getListing = function(pageNumber){

            $scope.states = [];
            if(pageNumber===undefined){
                pageNumber = '1';
            }

            datacontext.state.getAll(pageNumber).success(function(response) {
                    if(response.status == 'success')
                    {
                        $scope.currentPage = response.states.current_page;
                        $scope.totalPages = response.states.last_page;
                        // Pagination Range
                        var pages = [];

                        for(var i=1;i<=response.states.last_page;i++) {          
                            pages.push(i);
                        }

                        $scope.range = pages; 

                        $scope.states = response.states.data;
                    }
            });
        }

        $scope.addNewState = function(){

            vm.stateForm = {};
            
            $uibModal.open({
                templateUrl: 'addStateModal.html',
                size: 'lg',
                controller: 'ModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }


        $scope.edit = function(index,size){

            $scope.stateForm = angular.copy($scope.states[index]);
            $scope.editIndex = index;
            $uibModal.open({
                animation: true,
                templateUrl: 'editStateModal.html',
                size: 'lg',
                controller: 'ModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }

        $scope.delete = function(index){

            if(confirm('Are you sure that you want to permanently delete this record?'))
            {
                datacontext.state.deleteState($scope.states[index].id).success(function(result){
                    if(result.status == 'success')
                    { 
                        $scope.states.splice(index,1);
                    }
                });
         
            }

        }

        $scope.stateChanged = function (index,id) {

            if($scope.answers[id]){ //If it is checked
               $scope.selection.push(id);
            }
            else
            {
                var idx = $scope.selection.indexOf(id);
                $scope.selection.splice(idx, 1);
            }
        }

        $scope.deleteSelected = function(){

            if(confirm('Are you sure that you want to permanently delete selected records?'))
            {
                datacontext.state.deleteSelected($scope.selection).success(function(result){
                    if(result.status == 'success')
                    { 
                        for (var j = $scope.selection.length-1; j >= 0 ; j--) {
                            angular.forEach($scope.states, function(value, key) {
                                
                                if(value.id == $scope.selection[j])
                                {
                                    $scope.states.splice(key,1);
                                }

                            });
                           $scope.answers[$scope.selection[j]] = false;
                            $scope.stateChanged('',$scope.selection[j]);

                        }

                    }
                });
         
            }
        }

    };

    states.$inject = ['datacontext','$scope','$parse','$uibModal','$uibModalStack'];

    angular
        .module('app.states')
            .controller('States',states);

}());
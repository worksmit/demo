(function(){
    'use strict';

    var routes = function($stateProvider,$urlRouterProvider){
        
        $stateProvider
           .state('states',{
                url : '/states',
                abstract : true,
                template: '<ui-view />'
            })

            .state('states.list',{
                url : '/list',
                templateUrl : 'app/states/states.html',
                controller :  'States as vm',
                cache : false
            })
    };

    routes.$inject = ['$stateProvider','$urlRouterProvider'];

    angular
        .module('app.states')
        .config(routes);

}());
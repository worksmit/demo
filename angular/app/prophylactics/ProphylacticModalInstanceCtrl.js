(function(){

    'use strict';

    var ProphylacticModalInstanceCtrl = function(datacontext, $scope, $parse, $uibModalInstance){

        var vm = this;

         vm.prophylacticForm = {};


        $scope.update = function(prophylacticForm){

            datacontext.prophylactics.editProphylactics(prophylacticForm).success(function(result){
                if(result.status == 'success')
                { 
                    $scope.prophylactics[$scope.editIndex] = angular.copy(prophylacticForm);
                    $scope.submitted = false;
                    $scope.editProphylactics = false;
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('prophylacticFormEdit.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });
          
        }

        $scope.send = function(prophylacticForm){
            datacontext.prophylactics.saveProphylactics(prophylacticForm).success(function(result){
                if(result.status == 'success')
                {
                    prophylacticForm['id'] = result.id;
                    $scope.prophylactics.push(prophylacticForm);
                    vm.prophylacticForm = {};
                    $scope.submitted = false;
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('prophylacticForm.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });  

        }

      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
 
    };

    ProphylacticModalInstanceCtrl.$inject = ['datacontext','$scope','$parse','$uibModalInstance'];

    angular
        .module('app.prophylactics')
            .controller('ProphylacticModalInstanceCtrl',ProphylacticModalInstanceCtrl);

}());
(function(){

    'use strict';

    var prophylactics = function(datacontext,$scope,$parse,$uibModal,$uibModalStack){
    
        var vm = this;
        vm.head =  'Prophylactics';
        vm.prophylacticForm = {};
        $scope.prophylacticForm = {};
        $scope.prophylactics = [];
        $scope.editIndex = {};
        $scope.selection = [];
        $scope.answers = [];
        $scope.totalPages = 0;
        $scope.currentPage = 1;
        $scope.range = [];
    

        $scope.getListing = function(pageNumber){

            $scope.prophylactics = [];
            if(pageNumber===undefined){
                pageNumber = '1';
            }

            datacontext.prophylactics.getAll(pageNumber).success(function(response){

                 if(response.status == 'success')
                    {
                        $scope.currentPage = response.prophylactics.current_page;
                        $scope.totalPages = response.prophylactics.last_page;
                        // Pagination Range
                        var pages = [];

                        for(var i=1;i<=response.prophylactics.last_page;i++) {          
                            pages.push(i);
                        }

                        $scope.range = pages; 
        
                        $scope.prophylactics = response.prophylactics.data;
                    }
            });
        }

        $scope.addProphylactic = function(){

            vm.prophylacticForm = {};
            
            $uibModal.open({
                templateUrl: 'addProphylacticModal.html',
                size: 'lg',
                controller: 'ProphylacticModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }

        $scope.edit = function(index,size){

            $scope.prophylacticForm = angular.copy($scope.prophylactics[index]);
            $scope.editIndex = index;
            $uibModal.open({
                animation: true,
                templateUrl: 'editProphylacticModal.html',
                size: 'lg',
                controller: 'ProphylacticModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }

        $scope.delete = function(index){

            if(confirm('Are you sure that you want to permanently delete this record?'))
            {
                datacontext.prophylactics.deleteProphylactics($scope.prophylactics[index].id).success(function(result){
                    if(result.status == 'success')
                    { 
                        $scope.prophylactics.splice(index,1);
                    }
                });
         
            }

        }


        $scope.stateChanged = function (index,id) {

            if($scope.answers[id]){ //If it is checked
               $scope.selection.push(id);
            }
            else
            {
                var idx = $scope.selection.indexOf(id);
                $scope.selection.splice(idx, 1);
            }
        }

        $scope.deleteSelected = function(){

            if(confirm('Are you sure that you want to permanently delete selected records?'))
            {
                datacontext.prophylactics.deleteSelected($scope.selection).success(function(result){
                    if(result.status == 'success')
                    { 
                        for (var j = $scope.selection.length-1; j >= 0 ; j--) {
                            angular.forEach($scope.prophylactics, function(value, key) {
                                if(value.id == $scope.selection[j])
                                {
                                    
                                    $scope.prophylactics.splice(key,1);
                                }
                            });
                            $scope.answers[$scope.selection[j]] = false;
                            $scope.stateChanged('',$scope.selection[j]);

                        }
                    }
                });
         
            }
        }
        
        
    };

    prophylactics.$inject = ['datacontext','$scope','$parse','$uibModal','$uibModalStack'];

    angular
        .module('app.prophylactics')
            .controller('Prophylactics',prophylactics);

}());
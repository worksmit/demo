(function(){
    'use strict';

    var routes = function($stateProvider,$urlRouterProvider){
        
        $stateProvider
           .state('prophylactics',{
                url : '/prophylactics',
                abstract : true,
                template: '<ui-view />'
            })

            .state('prophylactics.list',{
                url : '/list',
                templateUrl : 'app/prophylactics/prophylactics.html',
                controller :  'Prophylactics as vm',
                cache : false
            })
    };

    routes.$inject = ['$stateProvider','$urlRouterProvider'];

    angular
        .module('app.prophylactics')
        .config(routes);

}());
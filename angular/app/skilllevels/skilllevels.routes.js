(function(){
    'use strict';

    var routes = function($stateProvider,$urlRouterProvider){
        
        $stateProvider
           .state('skilllevels',{
                url : '/skilllevels',
                abstract : true,
                template: '<ui-view />'
            })

            .state('skilllevels.list',{
                url : '/list',
                templateUrl : 'app/skilllevels/skilllevels.html',
                controller :  'skilllevels as vm',
                cache : false
            })
    };

    routes.$inject = ['$stateProvider','$urlRouterProvider'];

    angular
        .module('app.skilllevels')
        .config(routes);

}());
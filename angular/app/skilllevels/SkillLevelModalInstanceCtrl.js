(function(){

    'use strict';

    var SkillLevelModalInstanceCtrl = function(datacontext, $scope, $parse, $uibModalInstance){

        var vm = this;

         vm.skillLevelForm = {};

        $scope.update = function(skillLevelForm){

            datacontext.skilllevels.editSkillLevel(skillLevelForm).success(function(result){
                if(result.status == 'success')
                { 
                    $scope.skilllevels[$scope.editIndex] = angular.copy(skillLevelForm);
                    $scope.submitted = false;
                    $scope.editSkillLevel = false;
                    $uibModalInstance.close();

                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('skillLevelFormEdit.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });
          
        }

        $scope.send = function(skillLevelForm){
            datacontext.skilllevels.saveSkillLevel(skillLevelForm).success(function(result){
                if(result.status == 'success')
                {
                    skillLevelForm['id'] = result.id;
                    $scope.skilllevels.push(skillLevelForm);
                    vm.skillLevelForm = {};
                    $scope.submitted = false;
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('skillLevelForm.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });  

        }


      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
 
    };

    SkillLevelModalInstanceCtrl.$inject = ['datacontext','$scope','$parse','$uibModalInstance'];

    angular
        .module('app.skilllevels')
            .controller('SkillLevelModalInstanceCtrl',SkillLevelModalInstanceCtrl);

}());
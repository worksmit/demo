(function(){

    'use strict';

    var skilllevels = function(datacontext,$scope,$parse,$uibModal,$uibModalStack){
    
        var vm = this;
        vm.head =  'Skill Levels';
        vm.skillLevelForm = {};
        $scope.skillLevelForm = {};
        $scope.skilllevels = [];
        $scope.editIndex = {};
        $scope.selection = [];
        $scope.answers = [];
        $scope.totalPages = 0;
        $scope.currentPage = 1;
        $scope.range = [];
    

        $scope.getListing = function(pageNumber){

            $scope.skilllevels = [];
            if(pageNumber===undefined){
                pageNumber = '1';
            }

            datacontext.skilllevels.getAll(pageNumber).success(function(response){

                 if(response.status == 'success')
                    {
                        $scope.currentPage = response.skilllevels.current_page;
                        $scope.totalPages = response.skilllevels.last_page;
                        // Pagination Range
                        var pages = [];

                        for(var i=1;i<=response.skilllevels.last_page;i++) {          
                            pages.push(i);
                        }

                        $scope.range = pages; 

                        $scope.skilllevels = response.skilllevels.data;
 
                    }
            });
        }


        $scope.addNewSkillLevel = function(){

            vm.skillLevelForm = {};
            
            $uibModal.open({
                templateUrl: 'addSkillLevelModal.html',
                size: 'lg',
                controller: 'SkillLevelModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }

        $scope.edit = function(index,size){

            $scope.skillLevelForm = angular.copy($scope.skilllevels[index]);
            $scope.editIndex = index;
            $uibModal.open({
                animation: true,
                templateUrl: 'editSkillLevelModal.html',
                size: 'lg',
                controller: 'SkillLevelModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }

        $scope.delete = function(index){

            if(confirm('Are you sure that you want to permanently delete this record?'))
            {
                datacontext.skilllevels.deleteSkillLevel($scope.skilllevels[index].id).success(function(result){
                    if(result.status == 'success')
                    { 
                        $scope.skilllevels.splice(index,1);
                    }
                });
         
            }

        }

        $scope.stateChanged = function (index,id) {

            if($scope.answers[id]){ //If it is checked
               $scope.selection.push(id);
            }
            else
            {
                var idx = $scope.selection.indexOf(id);
                $scope.selection.splice(idx, 1);
            }
        }

        $scope.deleteSelected = function(){

            if(confirm('Are you sure that you want to permanently delete selected records?'))
            {
                datacontext.skilllevels.deleteSelected($scope.selection).success(function(result){
                    if(result.status == 'success')
                    { 
                        for (var j = $scope.selection.length-1; j >= 0 ; j--) {
                            angular.forEach($scope.skilllevels, function(value, key) {
                                if(value.id == $scope.selection[j])
                                {
                                    
                                    $scope.skilllevels.splice(key,1);
                                }
                            });
                            $scope.answers[$scope.selection[j]] = false;
                            $scope.stateChanged('',$scope.selection[j]);

                        }
                    }
                });
         
            }
        }
        
        
    };

    skilllevels.$inject = ['datacontext','$scope','$parse','$uibModal','$uibModalStack'];

    angular
        .module('app.skilllevels')
            .controller('skilllevels',skilllevels);

}());
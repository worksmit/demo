(function(){
    'use strict';

    var routes = function($stateProvider,$urlRouterProvider){
        
        $stateProvider
           .state('subcategories',{
                url : '/subcategories',
                abstract : true,
                template: '<ui-view />'
            })

            .state('subcategories.list',{
                url : '/list',
                templateUrl : 'app/subcategories/subcategories.html',
                controller :  'SubCategories as vm',
                cache : false
            })
    };

    routes.$inject = ['$stateProvider','$urlRouterProvider'];

    angular
        .module('app.subcategories')
        .config(routes);

}());
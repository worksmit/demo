(function(){

    'use strict';

    var subcategories = function(datacontext,$scope,$parse,$uibModal,$uibModalStack){
    
        var vm = this;
        vm.head =  'Sub Categories';
        vm.subCatForm = {};
        $scope.subCatForm = {};
        $scope.subcategories = [];     
        $scope.editIndex = {};
        $scope.selection = [];
        $scope.answers = [];
        $scope.totalPages = 0;
        $scope.currentPage = 1;
        $scope.range = [];


        $scope.getListing = function(pageNumber){

            $scope.subcategories = [];
            if(pageNumber===undefined){
                pageNumber = '1';
            }

            datacontext.subcategories.getAll(pageNumber).success(function(response){

                 if(response.status == 'success')
                    {
                        $scope.currentPage = response.data.current_page;
                        $scope.totalPages = response.data.last_page;
                        // Pagination Range
                        var pages = [];

                        for(var i=1;i<=response.data.last_page;i++) {          
                            pages.push(i);
                        }

                        $scope.range = pages; 

                        $scope.subcategories = response.data.data;

                    }
            });
        }

        $scope.addNewSubCategory = function(){

            vm.subCatForm = {};
            
            $uibModal.open({
                templateUrl: 'addSubCategoryModal.html',
                size: 'lg',
                controller: 'SubCategoryModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }


        $scope.edit = function(index,size){

            $scope.subCatForm = angular.copy($scope.subcategories[index]);
            $scope.editIndex = index;
            $uibModal.open({
                animation: true,
                templateUrl: 'editSubCategoryModal.html',
                size: 'lg',
                controller: 'SubCategoryModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }


        $scope.delete = function(index){

           

            if(confirm('Are you sure that you want to permanently delete this record?'))
            {
                datacontext.subcategories.deleteSubCategory($scope.subcategories[index].id).success(function(result){
                    if(result.status == 'success')
                    { 
                        $scope.subcategories.splice(index,1);
                    }
                });
         
            }

        }

        $scope.stateChanged = function (index,id) {

            if($scope.answers[id]){ //If it is checked
               $scope.selection.push(id);
            }
            else
            {
                var idx = $scope.selection.indexOf(id);
                $scope.selection.splice(idx, 1);
            }
        }

        $scope.deleteSelected = function(){

            if(confirm('Are you sure that you want to permanently delete selected records?'))
            {
                datacontext.subcategories.deleteSelected($scope.selection).success(function(result){
                    if(result.status == 'success')
                    { 
                        for (var j = $scope.selection.length-1; j >= 0 ; j--) {
                            angular.forEach($scope.subcategories, function(value, key) {
                                if(value.id == $scope.selection[j])
                                {
                                    
                                    $scope.subcategories.splice(key,1);
                                }
                            });
                            $scope.answers[$scope.selection[j]] = false;
                            $scope.stateChanged('',$scope.selection[j]);

                        }
                    }
                });
         
            }
        }
    };

    subcategories.$inject = ['datacontext','$scope','$parse','$uibModal','$uibModalStack'];

    angular
        .module('app.subcategories')
            .controller('SubCategories',subcategories);

}());
(function(){

    'use strict';

    var SubCategoryModalInstanceCtrl = function(datacontext, $scope, $parse, $uibModalInstance){

        var vm = this;

         vm.subCatForm = {};

        $scope.update = function(subCatForm){

            datacontext.subcategories.editSubCategory(subCatForm).success(function(result){
                if(result.status == 'success')
                { 
                    $scope.subcategories[$scope.editIndex] = angular.copy(subCatForm);
                    $scope.submitted = false;
                    $scope.editSubCategory = false;
                    $uibModalInstance.close();


                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('subCatFormEdit.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });
          
        }

        $scope.send = function(subCatForm){
            datacontext.subcategories.saveSubCategory(subCatForm).success(function(result){
     
                if(result.status == 'success')
                {
                    subCatForm['id'] = result.id;
                    $scope.subcategories.push(subCatForm);
                    vm.subCatForm = {};
                    $scope.submitted = false;
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('subCatForm.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });  
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
 
    };

    SubCategoryModalInstanceCtrl.$inject = ['datacontext','$scope','$parse','$uibModalInstance'];

    angular
        .module('app.subcategories')
            .controller('SubCategoryModalInstanceCtrl',SubCategoryModalInstanceCtrl);

}());
(function(){
    'use strict';

    var routes = function($stateProvider,$urlRouterProvider){
        
        $stateProvider
           .state('colleges',{
                url : '/colleges',
                abstract : true,
                template: '<ui-view />'
            })

            .state('colleges.list',{
                url : '/list',
                templateUrl : 'app/colleges/colleges.html',
                controller :  'Colleges as vm',
                cache : false
            })
    };

    routes.$inject = ['$stateProvider','$urlRouterProvider'];

    angular
        .module('app.colleges')
        .config(routes);

}());
(function(){

    'use strict';

    var CollegeModalInstanceCtrl = function(datacontext, $scope, $parse, $uibModalInstance){

        var vm = this;

         vm.collegeForm = {};

        $scope.update = function(collegeForm){

            delete collegeForm['state'];

            datacontext.college.editCollege(collegeForm).success(function(result){
                if(result.status == 'success')
                { 
                    $scope.colleges[$scope.editIndex] = angular.copy(collegeForm);
                    angular.forEach($scope.states,function(value1, key1) {
                    if($scope.states[key1].id == collegeForm.state_id)
                    {
                       $scope.colleges[$scope.editIndex]['state'] = $scope.states[key1];
                    }
                    });
                    $scope.editCollege = false;
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('collegeForm.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
            
                });
                }
            });
        }

        $scope.send = function(collegeForm){

            angular.forEach($scope.states, function(value, key) {
                
                if($scope.states[key].id == collegeForm.state_id)
                {
                    collegeForm['country_id'] = $scope.states[key].country_id;
                }
            });

            datacontext.college.saveCollege(collegeForm).success(function(result){
                if(result.status == 'success')
                {
                    collegeForm['id'] = result.id;
                    $scope.colleges.push(collegeForm);

                    angular.forEach($scope.states,function(value1, key1) {
                    if($scope.states[key1].id == collegeForm.state_id)
                    {
                       $scope.colleges[$scope.colleges.length-1]['state'] = $scope.states[key1];
                    }
                    });
                    vm.collegeForm = {};
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('collegeForm.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
            
                });
                }
            });     
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
 
    };

    CollegeModalInstanceCtrl.$inject = ['datacontext','$scope','$parse','$uibModalInstance'];

    angular
        .module('app.colleges')
            .controller('CollegeModalInstanceCtrl',CollegeModalInstanceCtrl);

}());
(function(){

    'use strict';

    var colleges = function(datacontext,$scope,$parse,$uibModal,$uibModalStack){
        var vm = this;
        vm.head =  'College List';
        vm.collegeForm = {};
        $scope.states = [];
        $scope.colleges = [];
        $scope.collegeForm = {};
        $scope.editCollege = false;
        $scope.editIndex = {};
        $scope.selection = [];
        $scope.answers = [];
        $scope.submitted = false;
        $scope.totalPages = 0;
        $scope.currentPage = 1;
        $scope.range = [];

        function init(){
              datacontext.state.getAll(null,'all').success(function(response) {
                if(response.status == 'success')
                {
                    $scope.states = response.states.data;
                }
            });
        }

        init();
  
        $scope.getListing = function(pageNumber){

            $scope.colleges = [];
            if(pageNumber===undefined){
                pageNumber = '1';
            }

            datacontext.college.getAll(pageNumber).success(function(response){

                 if(response.status == 'success')
                    {
                        $scope.currentPage = response.colleges.current_page;
                        $scope.totalPages = response.colleges.last_page;
                        // Pagination Range
                        var pages = [];

                        for(var i=1;i<=response.colleges.last_page;i++) {          
                            pages.push(i);
                        }

                        $scope.range = pages; 
                        
                        $scope.colleges = response.colleges.data;
       
                    }
            });
        }


        $scope.addNewCollege = function(){

            vm.collegeForm = {};
            
            $uibModal.open({
                templateUrl: 'addCollegeModal.html',
                size: 'lg',
                controller: 'CollegeModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }


        $scope.edit = function(index,size){

            $scope.collegeForm = angular.copy($scope.colleges[index]);
            $scope.editIndex = index;
            $uibModal.open({
                animation: true,
                templateUrl: 'editCollegeModal.html',
                size: 'lg',
                controller: 'CollegeModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }

        $scope.delete = function(index){
            if(confirm('Are you sure that you want to permanently delete this record?'))
            {
                datacontext.college.deleteCollege($scope.colleges[index].id).success(function(result){
                    if(result.status == 'success')
                    { 
                        $scope.colleges.splice(index,1);
                    }
                });
            }
        }

        $scope.stateChanged = function (index,id) {
            if($scope.answers[id]){ //If it is checked
               $scope.selection.push(id);
            }
            else
            {
                var idx = $scope.selection.indexOf(id);
                $scope.selection.splice(idx, 1);
            }
        }

        $scope.deleteSelected = function(){

            if(confirm('Are you sure that you want to permanently delete selected records?'))
            {
                datacontext.college.deleteSelected($scope.selection).success(function(result){
                    if(result.status == 'success')
                    { 
                        for (var j = $scope.selection.length-1; j >= 0 ; j--) {
                            angular.forEach($scope.colleges, function(value, key) {
                                
                                if(value.id == $scope.selection[j])
                                {
                                    $scope.colleges.splice(key,1);
                                }

                            });
                           $scope.answers[$scope.selection[j]] = false;
                            $scope.stateChanged('',$scope.selection[j]);

                        }

                    }
                });
         
            }
        }
    };

    colleges.$inject = ['datacontext','$scope','$parse','$uibModal','$uibModalStack'];
    angular
        .module('app.colleges')
            .controller('Colleges',colleges);

}());
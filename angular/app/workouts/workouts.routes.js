(function(){
    'use strict';

    var routes = function($stateProvider,$urlRouterProvider){
        
        $stateProvider
           .state('workouts',{
                url : '/workouts',
                abstract : true,
                template: '<ui-view />'
            })

            .state('workouts.list',{
                url : '/list',
                templateUrl : 'app/workouts/workouts.html',
                controller :  'Workouts as vm',
                cache : false
            })

            .state('workouts.add',{
                url : '/add',
                templateUrl : 'app/workouts/add-workouts.html',
                controller :  'Workouts as vm',
                cache : false
            })

            .state('workouts.edit',{
                url : '/edit?id={workoutId}',
                templateUrl : 'app/workouts/edit-workouts.html',
                controller :  'Workouts as vm',
                cache : false
            })
    };

    routes.$inject = ['$stateProvider','$urlRouterProvider'];

    angular
        .module('app.workouts')
        .config(routes);

}());
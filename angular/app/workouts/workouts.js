(function(){

    'use strict';

    var workouts = function(datacontext,$scope,$parse,$uibModal,$uibModalStack,$state){
    
        var vm = this;
        vm.head =  'Workouts';


        $scope.addNewWorkout = function(){

            $state.go('workouts.add');
        }

        $scope.edit = function(){

            $state.go('workouts.edit',{'workoutId':1});
        }
        
    };

    workouts.$inject = ['datacontext','$scope','$parse','$uibModal','$uibModalStack','$state'];

    angular
        .module('app.workouts')
            .controller('Workouts',workouts);

}());
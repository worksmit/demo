(function () {
    'use strict';

    var RepositoryOrganization = function($http,$rootScope,requestService){

        return {
            create: createRepo // factory function to create the repository
        };

        function createRepo(){
            var repo = {
                getAll: getAll,
                saveOrganization: saveOrganization,
                editOrganization: editOrganization,
                deleteOrganization: deleteOrganization,
                deleteSelected: deleteSelected
            };

            return repo;

            function getAll(pageNumber,perPage){

                var url = $rootScope.baseurl+'organizations?page='+pageNumber+'&per_page='+perPage;
                return requestService.getRequest(url);
            }


            function saveOrganization(organizationForm){

                var url = $rootScope.baseurl+'organizations';
                return requestService.postRequest(url,$.param(organizationForm),"POST");
            }

            function editOrganization(organizationForm){

                var url = $rootScope.baseurl+'organizations/'+organizationForm.id;
                return requestService.postRequest(url,$.param(organizationForm),"PUT");
            }

            function deleteOrganization(id){

                var url = $rootScope.baseurl+'organizations/'+id;
                return requestService.postRequest(url,null,"DELETE");
            }

            function deleteSelected(selectedData){

                var url = $rootScope.baseurl+'organizations/bulkdestroy';
                return requestService.postRequest(url,$.param({"id" : selectedData}),"POST");        
            }
        }

    };

    RepositoryOrganization.$inject = ['$http','$rootScope','requestService'];

    angular
        .module('app')
            .factory('repository.organization', RepositoryOrganization);
}());
(function () {
    'use strict';

    var RepositorySpecificSkills = function($http,$rootScope,requestService){
        
        return {
            create: createRepo // factory function to create the repository
        };
        
        function createRepo(){
            
            var repo = {
                getAll: getAll,
                saveSpecificSkill:saveSpecificSkill,
                editSpecificSkill:editSpecificSkill,
                deleteSpecificSkill:deleteSpecificSkill,
                deleteSelected: deleteSelected
            };
            
            return repo;
            
            
            function getAll(pageNumber,perPage){
                
                var url = $rootScope.baseurl+'specificskills?page='+pageNumber+'&per_page='+perPage;
                return requestService.getRequest(url);
                
            }
             function saveSpecificSkill(specificSkillForm){

                var url = $rootScope.baseurl+'specificskills';
                return requestService.postRequest(url,$.param(specificSkillForm),"POST");
            }

            function editSpecificSkill(specificSkillForm){
    
                var url = $rootScope.baseurl+'specificskills/'+specificSkillForm.id;
                return requestService.postRequest(url,$.param(specificSkillForm),"PUT");
            }

            function deleteSpecificSkill(id){

                var url = $rootScope.baseurl+'specificskills/'+id;
                return requestService.postRequest(url,null,"DELETE");
            }

            function deleteSelected(selectedData){

                var url = $rootScope.baseurl+'specificskills/bulkdestroy';
                return requestService.postRequest(url,$.param({"id" : selectedData}),"POST");
            }
            
        }
        
        
        
    };

    RepositorySpecificSkills.$inject = ['$http','$rootScope','requestService'];

    angular
        .module('app')
            .factory('repository.specificskills', RepositorySpecificSkills);
}());
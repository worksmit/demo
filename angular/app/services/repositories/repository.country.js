(function () {
    'use strict';

    var RepositoryCountry = function($http,$rootScope,requestService){

        return {
            create: createRepo // factory function to create the repository
        };

        function createRepo(){

            var repo = {
                getAll: getAll,
                saveCountry:saveCountry,
                editCountry:editCountry,
                deleteCountry:deleteCountry,
                deleteSelected: deleteSelected
            };

            return repo;

            function getAll(pageNumber,perPage){

                var url = $rootScope.baseurl+'country?page='+pageNumber+'&per_page='+perPage;
                return requestService.getRequest(url);

            }

            function saveCountry(countryForm){

                var url = $rootScope.baseurl+'country';
                return requestService.postRequest(url,$.param(countryForm),"POST");
            }

            function editCountry(countryForm){

                var url = $rootScope.baseurl+'country/'+countryForm.id;
                return requestService.postRequest(url,$.param(countryForm),"PUT");
            }

            function deleteCountry(id){

                var url = $rootScope.baseurl+'country/'+id;
                return requestService.postRequest(url,null,"DELETE");
            }

            function deleteSelected(selectedData){

                var url = $rootScope.baseurl+'country/bulkdestroy';
                return requestService.postRequest(url,$.param({"id" : selectedData}),"POST");
            }

        }
    };

    RepositoryCountry.$inject = ['$http','$rootScope','requestService'];

    angular
        .module('app')
            .factory('repository.country', RepositoryCountry);
}());
(function () {
    'use strict';

    var RepositoryBodyParts = function($http,$rootScope,requestService){
        
        return {
            create: createRepo // factory function to create the repository
        };
        
        function createRepo(){
            
            var repo = {
                getAll: getAll,
                saveBodyPart:saveBodyPart,
                editBodyPart:editBodyPart,
                deleteBodyPart:deleteBodyPart,
                deleteSelected: deleteSelected
            };
            
            return repo;
            
            
            function getAll(pageNumber,perPage){
    
                var url = $rootScope.baseurl+'bodyparts?page='+pageNumber+'&per_page='+perPage;
                return requestService.getRequest(url);
                
            }
            
            function saveBodyPart(bodyPartForm){

                  var url = $rootScope.baseurl+'bodyparts';
                  return requestService.postRequest(url,$.param(bodyPartForm),"POST");
            }

            function editBodyPart(bodyPartForm){
   
                  var url = $rootScope.baseurl+'bodyparts/'+bodyPartForm.id;
                  return requestService.postRequest(url,$.param(bodyPartForm),"PUT");
            }

            function deleteBodyPart(id){
        
                  var url = $rootScope.baseurl+'bodyparts/'+id;
                  return requestService.postRequest(url,null,"DELETE");
            }

            function deleteSelected(selectedData){

                  var url = $rootScope.baseurl+'bodyparts/bulkdestroy';
                  return requestService.postRequest(url,$.param({"id" : selectedData}),"POST");
            }
            
        }
        
        
        
    };

    RepositoryBodyParts.$inject = ['$http','$rootScope','requestService'];

    angular
        .module('app')
            .factory('repository.bodyparts', RepositoryBodyParts);
}());
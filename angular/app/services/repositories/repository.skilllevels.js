(function () {
    'use strict';

    var RepositorySkillLevels = function($http,$rootScope,requestService){
        
        return {
            create: createRepo // factory function to create the repository
        };
        
        function createRepo(){
            
            var repo = {
                getAll: getAll,
                saveSkillLevel:saveSkillLevel,
                editSkillLevel:editSkillLevel,
                deleteSkillLevel:deleteSkillLevel,
                deleteSelected: deleteSelected
            };
            
            return repo;
            
            
            function getAll(pageNumber,perPage){

                var url = $rootScope.baseurl+'skilllevels?page='+pageNumber+'&per_page='+perPage;
                return requestService.getRequest(url);
            }
            function saveSkillLevel(skillLevelForm){

                var url = $rootScope.baseurl+'skilllevels';
                return requestService.postRequest(url,$.param(skillLevelForm),"POST");
            }

            function editSkillLevel(skillLevelForm){
           
                var url = $rootScope.baseurl+'skilllevels/'+skillLevelForm.id;
                return requestService.postRequest(url,$.param(skillLevelForm),"PUT");
            }

            function deleteSkillLevel(id){

                var url = $rootScope.baseurl+'skilllevels/'+id;
                return requestService.postRequest(url,null,"DELETE");
            }

            function deleteSelected(selectedData){

                var url = $rootScope.baseurl+'skilllevels/bulkdestroy';
                return requestService.postRequest(url,$.param({"id" : selectedData}),"POST");    
            }
            
        }
    };

    RepositorySkillLevels.$inject = ['$http','$rootScope','requestService'];

    angular
        .module('app')
            .factory('repository.skilllevels', RepositorySkillLevels);
}());
(function () {
    'use strict';

    var RepositoryState = function($http,$rootScope,requestService){

        return {
            create: createRepo // factory function to create the repository
        };

        function createRepo(){
            var repo = {
                getAll: getAll,
                saveState: saveState,
                editState: editState,
                deleteState: deleteState,
                deleteSelected:deleteSelected
            };

            return repo;

            function getAll(pageNumber,perPage){

                var url = $rootScope.baseurl+'states?page='+pageNumber+'&per_page='+perPage;
                return requestService.getRequest(url);
            }

             function saveState(stateForm){

                var url = $rootScope.baseurl+'states';
                return requestService.postRequest(url,$.param(stateForm),"POST");
            }

            function editState(stateForm){

                var url = $rootScope.baseurl+'states/'+stateForm.id;
                return requestService.postRequest(url,$.param(stateForm),"PUT");
            }

            function deleteState(id){

                var url = $rootScope.baseurl+'states/'+id;
                return requestService.postRequest(url,null,"DELETE");
            }

            function deleteSelected(selectedData){

                var url = $rootScope.baseurl+'state/bulkdestroy';
                return requestService.postRequest(url,$.param({"id" : selectedData}),"POST");
            }

        }

    };

    RepositoryState.$inject = ['$http','$rootScope','requestService'];

    angular
        .module('app')
            .factory('repository.state', RepositoryState);
}());
(function () {
    'use strict';

    var RepositoryCategories = function($http,$rootScope,requestService){
        
        return {
            create: createRepo // factory function to create the repository
        };
        
        function createRepo(){
            
            var repo = {
                getAll: getAll,
                saveCategory:saveCategory,
                editCategory:editCategory,
                deleteCategory:deleteCategory,
                deleteSelected: deleteSelected
            };
            
            return repo;
            
            
            function getAll(pageNumber,perPage){
                
                
                var url = $rootScope.baseurl+'categories?page='+pageNumber+'&per_page='+perPage;
                return requestService.getRequest(url);
            }
            
            function saveCategory(catForm){

                  var url = $rootScope.baseurl+'categories';
                  return requestService.postRequest(url,$.param(catForm),"POST");
            }

            function editCategory(catForm){

                  var url =  $rootScope.baseurl+'categories/'+catForm.id;
                  return requestService.postRequest(url,$.param(catForm),"PUT");
            }

            function deleteCategory(id){

                  var url =  $rootScope.baseurl+'categories/'+id;
                  return requestService.postRequest(url,null,"DELETE");
            }

            function deleteSelected(selectedData){

                  var url =  $rootScope.baseurl+'categories/bulkdestroy';
                  return requestService.postRequest(url,$.param({"id" : selectedData}),"POST");
            }
            
        }
    };

    RepositoryCategories.$inject = ['$http','$rootScope','requestService'];

    angular
        .module('app')
            .factory('repository.categories', RepositoryCategories);
}());
(function () {
    'use strict';

    var RepositorySubCategories = function($http,$rootScope,requestService){
        
        return {
            create: createRepo // factory function to create the repository
        };
        
        function createRepo(){
            
            var repo = {
                getAll: getAll,
                saveSubCategory:saveSubCategory,
                editSubCategory:editSubCategory,
                deleteSubCategory:deleteSubCategory,
                deleteSelected: deleteSelected
            };
            
            return repo;
            
            
            function getAll(pageNumber,perPage){
                
                var url = $rootScope.baseurl+'subcategories?page='+pageNumber+'&per_page='+perPage;
                return requestService.getRequest(url);
            }
            
            function saveSubCategory(subCatForm){

                var url = $rootScope.baseurl+'subcategories';
                return requestService.postRequest(url,$.param(subCatForm),"POST");
            }

            function editSubCategory(subCatForm){

                var url = $rootScope.baseurl+'subcategories/'+subCatForm.id;
                return requestService.postRequest(url,$.param(subCatForm),"PUT");
            }

            function deleteSubCategory(id){
     
                var url = $rootScope.baseurl+'subcategories/'+id;
                return requestService.postRequest(url,null,"DELETE");
            }

            function deleteSelected(selectedData){

                var url = $rootScope.baseurl+'subcategories/bulkdestroy';
                return requestService.postRequest(url,$.param({"id" : selectedData}),"POST");
            }
            
        }
    };

    RepositorySubCategories.$inject = ['$http','$rootScope','requestService'];

    angular
        .module('app')
            .factory('repository.subcategories', RepositorySubCategories);
}());
(function () {
    'use strict';

    var RepositoryCollege = function($http,$rootScope,requestService){
   
        return {
            create: createRepo // factory function to create the repository
        };

        function createRepo(){
            var repo = {
                getAll: getAll,
                saveCollege:saveCollege,
                editCollege:editCollege,
                deleteSelected:deleteSelected,
                deleteCollege:deleteCollege
            };

            return repo;

            function getAll(pageNumber,perPage){
  
                var url = $rootScope.baseurl+'colleges?page='+pageNumber+'&per_page='+perPage;
                return requestService.getRequest(url);
            }

            function saveCollege(collegeForm){

                var url = $rootScope.baseurl+'colleges';
                return requestService.postRequest(url,$.param(collegeForm),"POST");       
            }

            function editCollege(collegeForm){
    
                var url = $rootScope.baseurl+'colleges/'+collegeForm.id;
                return requestService.postRequest(url,$.param(collegeForm),"PUT");      
            }

            function deleteCollege(id){

                var url = $rootScope.baseurl+'colleges/'+id;
                return requestService.postRequest(url,null,"DELETE");
            }

            function deleteSelected(selectedData){

                var url = $rootScope.baseurl+'colleges/bulkdestroy';
                return requestService.postRequest(url,$.param({"id" : selectedData}),"POST");       
            }
        }
    };

    RepositoryCollege.$inject = ['$http','$rootScope','requestService'];

    angular
        .module('app')
            .factory('repository.college', RepositoryCollege);
}());
(function () {
    'use strict';

    var RepositoryTrainingLocations = function($http,$rootScope,requestService){
        
        return {
            create: createRepo // factory function to create the repository
        };
        
        function createRepo(){
            
            var repo = {
                getAll: getAll,
                saveTrainingLocation:saveTrainingLocation,
                editTrainingLocation:editTrainingLocation,
                deleteTrainingLocation:deleteTrainingLocation,
                deleteSelected: deleteSelected
            };
            
            return repo;
            
            
            function getAll(pageNumber,perPage){
                 
                var url = $rootScope.baseurl+'traininglocations?page='+pageNumber+'&per_page='+perPage;
                return requestService.getRequest(url);
            }
              function saveTrainingLocation(trainingLocationForm){

                var url = $rootScope.baseurl+'traininglocations';
                return requestService.postRequest(url,$.param(trainingLocationForm),"POST");
            }

            function editTrainingLocation(trainingLocationForm){

                var url = $rootScope.baseurl+'traininglocations/'+trainingLocationForm.id;
                return requestService.postRequest(url,$.param(trainingLocationForm),"PUT");
            }

            function deleteTrainingLocation(id){

                var url = $rootScope.baseurl+'traininglocations/'+id;
                return requestService.postRequest(url,null,"DELETE");
            }

            function deleteSelected(selectedData){

                var url =  $rootScope.baseurl+'traininglocations/bulkdestroy';
                return requestService.postRequest(url,$.param({"id" : selectedData}),"POST");
            }
            
        }
        
        
        
    };

    RepositoryTrainingLocations.$inject = ['$http','$rootScope','requestService'];

    angular
        .module('app')
            .factory('repository.traininglocations', RepositoryTrainingLocations);
}());
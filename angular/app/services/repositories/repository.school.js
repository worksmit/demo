(function () {
    'use strict';

    var RepositorySchool = function($http,$q,$rootScope,requestService){

        return {
            create: createRepo, // factory function to create the repository
        };

        function createRepo(){

            var repo = {
                getAll: getAll,
                saveSchool: saveSchool,
                editSchool: editSchool,
                deleteSchool: deleteSchool,
                deleteSelected: deleteSelected
            };

            return repo;

            function getAll(pageNumber,perPage){

                var url = $rootScope.baseurl+'schools?page='+pageNumber+'&per_page='+perPage;
                return requestService.getRequest(url);
              
            }

             function saveSchool(schoolForm){

                var url = $rootScope.baseurl+'schools';
                return requestService.postRequest(url,$.param(schoolForm),"POST");

            }

            function editSchool(schoolForm){

                var url = $rootScope.baseurl+'schools/'+schoolForm.id;
                return requestService.postRequest(url,$.param(schoolForm),"PUT");
            }

            function deleteSchool(id){

                var url = $rootScope.baseurl+'schools/'+id;
                return requestService.postRequest(url,null,"DELETE");
            }

            function deleteSelected(selectedData){

                var url = $rootScope.baseurl+'schools/bulkdestroy';
                return requestService.postRequest(url,$.param({"id" : selectedData}),"POST");
            }

        }



    };

    RepositorySchool.$inject = ['$http','$q','$rootScope','requestService'];

    angular
        .module('app')
            .factory('repository.school', RepositorySchool);
}());


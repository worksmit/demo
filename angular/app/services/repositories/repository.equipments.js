(function () {
    'use strict';

    var RepositoryEquipments = function($http,$rootScope,requestService){
        
        return {
            create: createRepo // factory function to create the repository
        };
        
        function createRepo(){
            
            var repo = {
                getAll: getAll,
                saveEquipment:saveEquipment,
                editEquipment:editEquipment,
                deleteEquipment:deleteEquipment,
                deleteSelected: deleteSelected
            };
            
            return repo;
            
            
            function getAll(pageNumber,perPage){

                var url = $rootScope.baseurl+'equipments?page='+pageNumber+'&per_page='+perPage;
                return requestService.getRequest(url);
            }
            
            function saveEquipment(equipmentForm){

                var url = $rootScope.baseurl+'equipments';
                return requestService.postRequest(url,$.param(equipmentForm),"POST");
            }

            function editEquipment(equipmentForm){

                var url = $rootScope.baseurl+'equipments/'+equipmentForm.id;
                return requestService.postRequest(url,$.param(equipmentForm),"PUT");
            }

            function deleteEquipment(id){

                var url = $rootScope.baseurl+'equipments/'+id;
                return requestService.postRequest(url,null,"DELETE");

            }

            function deleteSelected(selectedData){

                var url = $rootScope.baseurl+'equipments/bulkdestroy';
                return requestService.postRequest(url,$.param({"id" : selectedData}),"POST");
            }
            
        }
    };

    RepositoryEquipments.$inject = ['$http','$rootScope','requestService'];

    angular
        .module('app')
            .factory('repository.equipments', RepositoryEquipments);
}());
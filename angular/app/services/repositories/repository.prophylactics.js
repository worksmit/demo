(function () {
    'use strict';

    var RepositoryProphylactics = function($http,$rootScope,requestService){

        return {
            create: createRepo // factory function to create the repository
        };
        
        function createRepo(){
            
            var repo = {
                getAll: getAll,
                saveProphylactics:saveProphylactics,
                editProphylactics:editProphylactics,
                deleteProphylactics:deleteProphylactics,
                deleteSelected: deleteSelected
            };
            
            return repo;
            
            
            function getAll(pageNumber,perPage){

                var url = $rootScope.baseurl+'prophylactics?page='+pageNumber+'&per_page='+perPage;
                return requestService.getRequest(url);
            }
             function saveProphylactics(prophylacticsForm){

                var url = $rootScope.baseurl+'prophylactics';
                return requestService.postRequest(url,$.param(prophylacticsForm),"POST");
            }

            function editProphylactics(prophylacticsForm){

                var url = $rootScope.baseurl+'prophylactics/'+prophylacticsForm.id;
                return requestService.postRequest(url,$.param(prophylacticsForm),"PUT");
            }

            function deleteProphylactics(id){
            
                var url =  $rootScope.baseurl+'prophylactics/'+id;
                return requestService.postRequest(url,null,"DELETE");
            }

            function deleteSelected(selectedData){

                var url =  $rootScope.baseurl+'prophylactics/bulkdestroy';
                return requestService.postRequest(url,$.param({"id" : selectedData}),"POST");
            }
            
        }
        
        
        
    };

    RepositoryProphylactics.$inject = ['$http','$rootScope','requestService'];

    angular
        .module('app')
            .factory('repository.prophylactics', RepositoryProphylactics);
}());
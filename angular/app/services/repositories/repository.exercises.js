(function () {
    'use strict';

    var RepositoryExercise = function($http,$rootScope,requestService){

        return {
            create: createRepo // factory function to create the repository
        };

        function createRepo(){
            var repo = {
                getAll: getAll,
                getAllMasters:getAllMasters,
                saveExercise: saveExercise,
                editExercise: editExercise,
                deleteExercise: deleteExercise,
                deleteSelected: deleteSelected,
                getExerciseById:getExerciseById,
                removeFiles:removeFiles
            };

            return repo;

            function getAll(pageNumber,perPage){

                var url = $rootScope.baseurl+'exercises?page='+pageNumber+'&per_page='+perPage;
                return requestService.getRequest(url);
            }

            function getAllMasters(){

                var url = $rootScope.baseurl+'exercises/masters';
                return requestService.getRequest(url);
            }


            function getExerciseById(id){

                var url = $rootScope.baseurl+'exercises/'+id+'/edit';
                return requestService.getRequest(url);
            }

            function saveExercise(exerciseForm){
                
               console.log(exerciseForm);
                var url = $rootScope.baseurl+'exercises';
                return requestService.postRequest(url,$.param(exerciseForm),"POST");
            }

            function editExercise(exerciseForm){

                var url = $rootScope.baseurl+'exercises/'+exerciseForm.id;
                return requestService.postRequest(url,$.param(exerciseForm),"PUT");
            }

            function deleteExercise(id){

                var url = $rootScope.baseurl+'exercises/'+id;
                return requestService.postRequest(url,null,"DELETE");
            }

            function deleteSelected(selectedData){

                var url = $rootScope.baseurl+'exercises/bulkdestroy';
                return requestService.postRequest(url,$.param({"id" : selectedData}),"POST");        
            }

            function removeFiles(file_name){

                console.log(file_name);
                var url = $rootScope.baseurl+'exercises/delete_temp_asset';
                return requestService.postRequest(url,$.param({"asset_id" : file_name}),"POST");
            }
        }

    };

    RepositoryExercise.$inject = ['$http','$rootScope','requestService'];

    angular
        .module('app')
            .factory('repository.exercises', RepositoryExercise);
}());
(function(){

    'use strict';

    var specificskills = function(datacontext,$scope,$parse,$uibModal,$uibModalStack){
    
        var vm = this;
        vm.head =  'Specific Skills';
        vm.specificSkillForm = {};
        $scope.specificSkillForm = {};
        $scope.specificskills = [];
        $scope.editIndex = {};
        $scope.selection = [];
        $scope.answers = [];
        $scope.totalPages = 0;
        $scope.currentPage = 1;
        $scope.range = [];
    

        $scope.getListing = function(pageNumber){

            $scope.specificskills = [];
            if(pageNumber===undefined){
                pageNumber = '1';
            }

            datacontext.specificskills.getAll(pageNumber).success(function(response){

                 if(response.status == 'success')
                    {
                        $scope.currentPage = response.specificskills.current_page;
                        $scope.totalPages = response.specificskills.last_page;
                        // Pagination Range
                        var pages = [];

                        for(var i=1;i<=response.specificskills.last_page;i++) {          
                            pages.push(i);
                        }

                        $scope.range = pages; 
    
                        $scope.specificskills = response.specificskills.data;
                    }
            });
        }


        $scope.addSpecificSkill = function(){

            vm.specificSkillForm = {};
            
            $uibModal.open({
                templateUrl: 'addSpecificSkillModal.html',
                size: 'lg',
                controller: 'SpecificSkillModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }

        $scope.edit = function(index,size){

            $scope.specificSkillForm = angular.copy($scope.specificskills[index]);
            $scope.editIndex = index;
            $uibModal.open({
                animation: true,
                templateUrl: 'editSpecificSkillModal.html',
                size: 'lg',
                controller: 'SpecificSkillModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }

        $scope.delete = function(index){

           

            if(confirm('Are you sure that you want to permanently delete this record?'))
            {
                datacontext.specificskills.deleteSpecificSkill($scope.specificskills[index].id).success(function(result){
                    if(result.status == 'success')
                    { 
                        $scope.specificskills.splice(index,1);
                    }
                });
         
            }

        }

        $scope.stateChanged = function (index,id) {

            if($scope.answers[id]){ //If it is checked
               $scope.selection.push(id);
            }
            else
            {
                var idx = $scope.selection.indexOf(id);
                $scope.selection.splice(idx, 1);
            }
        }

        $scope.deleteSelected = function(){

            if(confirm('Are you sure that you want to permanently delete selected records?'))
            {
                datacontext.specificskills.deleteSelected($scope.selection).success(function(result){
                    if(result.status == 'success')
                    { 
                        for (var j = $scope.selection.length-1; j >= 0 ; j--) {
                            angular.forEach($scope.specificskills, function(value, key) {
                                if(value.id == $scope.selection[j])
                                {
                                    
                                    $scope.specificskills.splice(key,1);
                                }
                            });
                            $scope.answers[$scope.selection[j]] = false;
                            $scope.stateChanged('',$scope.selection[j]);

                        }
                    }
                });
         
            }
        }
        
    };

    specificskills.$inject = ['datacontext','$scope','$parse','$uibModal','$uibModalStack'];

    angular
        .module('app.specificskills')
            .controller('specificskills',specificskills);

}());
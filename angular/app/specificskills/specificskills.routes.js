(function(){
    'use strict';

    var routes = function($stateProvider,$urlRouterProvider){
        
        $stateProvider
           .state('specificskills',{
                url : '/specificskills',
                abstract : true,
                template: '<ui-view />'
            })

            .state('specificskills.list',{
                url : '/list',
                templateUrl : 'app/specificskills/specificskills.html',
                controller :  'specificskills as vm',
                cache : false
            })
    };

    routes.$inject = ['$stateProvider','$urlRouterProvider'];

    angular
        .module('app.specificskills')
        .config(routes);

}());
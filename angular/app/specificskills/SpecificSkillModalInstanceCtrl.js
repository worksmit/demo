(function(){

    'use strict';

    var SpecificSkillModalInstanceCtrl = function(datacontext, $scope, $parse, $uibModalInstance){

        var vm = this;

         vm.specificSkillForm = {};


        $scope.update = function(specificSkillForm){

            datacontext.specificskills.editSpecificSkill(specificSkillForm).success(function(result){
                if(result.status == 'success')
                { 
                    $scope.specificskills[$scope.editIndex] = angular.copy(specificSkillForm);
                    $scope.submitted = false;
                    $scope.editSpecificSkill = false;
                    $uibModalInstance.close();

                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('specificSkillFormEdit.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });
          
        }

        $scope.send = function(specificSkillForm){
            datacontext.specificskills.saveSpecificSkill(specificSkillForm).success(function(result){
                if(result.status == 'success')
                {
                    specificSkillForm['id'] = result.id;
                    $scope.specificskills.push(specificSkillForm);
                    vm.specificSkillForm = {};
                    $scope.submitted = false;
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('specificSkillForm.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });  

        }

      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
 
    };

    SpecificSkillModalInstanceCtrl.$inject = ['datacontext','$scope','$parse','$uibModalInstance'];

    angular
        .module('app.specificskills')
            .controller('SpecificSkillModalInstanceCtrl',SpecificSkillModalInstanceCtrl);

}());
(function(){

    'use strict';

    var bodyparts = function(datacontext,$scope,$parse,$uibModal,$uibModalStack){
    
        var vm = this;
        vm.head =  'Body Parts';
        vm.bodyPartForm = {};
        $scope.bodyPartForm = {};
        $scope.bodyparts = [];
        $scope.editIndex = {};
        $scope.selection = [];
        $scope.answers = [];
        $scope.totalPages = 0;
        $scope.currentPage = 1;
        $scope.range = [];
        
        $scope.getListing = function(pageNumber){

            $scope.bodyparts = [];
            if(pageNumber===undefined){
                pageNumber = '1';
            }

            datacontext.bodyparts.getAll(pageNumber).success(function(response){

                 if(response.status == 'success')
                    {
                        $scope.currentPage = response.bodyparts.current_page;
                        $scope.totalPages = response.bodyparts.last_page;
                        // Pagination Range
                        var pages = [];

                        for(var i=1;i<=response.bodyparts.last_page;i++) {          
                            pages.push(i);
                        }

                        $scope.range = pages; 
                        $scope.bodyparts = response.bodyparts.data;
    
                    }
            });
        }
        
        $scope.addNewBodypart = function(){

            vm.bodyPartForm = {};
            
            $uibModal.open({
                templateUrl: 'addBodypartModal.html',
                size: 'lg',
                controller: 'BodypartModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }

        $scope.edit = function(index,size){

            $scope.bodyPartForm = angular.copy($scope.bodyparts[index]);
            $scope.editIndex = index;
            $uibModal.open({
                animation: true,
                templateUrl: 'editBodypartModal.html',
                size: 'lg',
                controller: 'BodypartModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }

        $scope.delete = function(index){

           

            if(confirm('Are you sure that you want to permanently delete this record?'))
            {
                datacontext.bodyparts.deleteBodyPart($scope.bodyparts[index].id).success(function(result){
                    if(result.status == 'success')
                    { 
                        $scope.bodyparts.splice(index,1);
                    }
                });
         
            }

        }

        $scope.stateChanged = function (index,id) {

            if($scope.answers[id]){ //If it is checked
               $scope.selection.push(id);
            }
            else
            {
                var idx = $scope.selection.indexOf(id);
                $scope.selection.splice(idx, 1);
            }
        }

        $scope.deleteSelected = function(){

            if(confirm('Are you sure that you want to permanently delete selected records?'))
            {
                datacontext.bodyparts.deleteSelected($scope.selection).success(function(result){
                    if(result.status == 'success')
                    { 
                        for (var j = $scope.selection.length-1; j >= 0 ; j--) {
                            angular.forEach($scope.bodyparts, function(value, key) {
                                if(value.id == $scope.selection[j])
                                {
                                    
                                    $scope.bodyparts.splice(key,1);
                                }
                            });
                            $scope.answers[$scope.selection[j]] = false;
                            $scope.stateChanged('',$scope.selection[j]);

                        }
                    }
                });
         
            }
        }
        
    };

    bodyparts.$inject = ['datacontext','$scope','$parse','$uibModal','$uibModalStack'];

    angular
        .module('app.bodyparts')
            .controller('Bodyparts',bodyparts);

}());
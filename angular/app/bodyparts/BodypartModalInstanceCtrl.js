(function(){

    'use strict';

    var BodypartModalInstanceCtrl = function(datacontext, $scope, $parse, $uibModalInstance){

        var vm = this;

         vm.bodyPartForm = {};

        $scope.update = function(bodyPartForm){

            datacontext.bodyparts.editBodyPart(bodyPartForm).success(function(result){
                if(result.status == 'success')
                { 
                    $scope.bodyparts[$scope.editIndex] = angular.copy(bodyPartForm);
                    $scope.submitted = false;
                    $uibModalInstance.close();

                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('bodyPartFormEdit.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });
          
        }

        $scope.send = function(bodyPartForm){
            datacontext.bodyparts.saveBodyPart(bodyPartForm).success(function(result){
               
                if(result.status == 'success')
                {
                    bodyPartForm['id'] = result.id;
                    $scope.bodyparts.push(bodyPartForm);
                    vm.bodyPartForm = {};
                    $scope.submitted = false;
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('bodyPartForm.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });  
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
 
    };

    BodypartModalInstanceCtrl.$inject = ['datacontext','$scope','$parse','$uibModalInstance'];

    angular
        .module('app.bodyparts')
            .controller('BodypartModalInstanceCtrl',BodypartModalInstanceCtrl);

}());
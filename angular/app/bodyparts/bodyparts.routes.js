(function(){
    'use strict';

    var routes = function($stateProvider,$urlRouterProvider){
        
        $stateProvider
           .state('bodyparts',{
                url : '/bodyparts',
                abstract : true,
                template: '<ui-view />'
            })

            .state('bodyparts.list',{
                url : '/list',
                templateUrl : 'app/bodyparts/bodyparts.html',
                controller :  'Bodyparts as vm',
                cache : false
            })
    };

    routes.$inject = ['$stateProvider','$urlRouterProvider'];

    angular
        .module('app.bodyparts')
        .config(routes);

}());
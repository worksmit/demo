(function(){

    'use strict';

    var TrainingLocationModalInstanceCtrl = function(datacontext, $scope, $parse, $uibModalInstance){

        var vm = this;

         vm.bodyPartForm = {};

        $scope.update = function(trainingLocationsForm){

            datacontext.traininglocations.editTrainingLocation(trainingLocationsForm).success(function(result){
                if(result.status == 'success')
                { 
                    $scope.traininglocations[$scope.editIndex] = angular.copy(trainingLocationsForm);
                    $scope.submitted = false;
                    $scope.editTrainingLocation = false;
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('trainingLocationsFormEdit.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });
          
        }

        
        $scope.send = function(trainingLocationsForm){
            datacontext.traininglocations.saveTrainingLocation(trainingLocationsForm).success(function(result){
                if(result.status == 'success')
                {
                    trainingLocationsForm['id'] = result.id;
                    $scope.traininglocations.push(trainingLocationsForm);
                    vm.trainingLocationsForm = {};
                    $scope.submitted = false;
                    $uibModalInstance.close();
                }
                if(result.status == 'error')
                {
                angular.forEach(result.error,function(value,key){

                    var serverMessage = $parse('trainingLocationsForm.'+key+'.$error.serverMessage');
                    angular.forEach(value,function(value1,key1){
                        serverMessage.assign($scope, value1);
                    });
                });
                }
            });  

        }

      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
 
    };

    TrainingLocationModalInstanceCtrl.$inject = ['datacontext','$scope','$parse','$uibModalInstance'];

    angular
        .module('app.traininglocations')
            .controller('TrainingLocationModalInstanceCtrl',TrainingLocationModalInstanceCtrl);

}());
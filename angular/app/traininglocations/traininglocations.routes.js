(function(){
    'use strict';

    var routes = function($stateProvider,$urlRouterProvider){
        
        $stateProvider
           .state('traininglocations',{
                url : '/traininglocations',
                abstract : true,
                template: '<ui-view />'
            })

            .state('traininglocations.list',{
                url : '/list',
                templateUrl : 'app/traininglocations/traininglocations.html',
                controller :  'traininglocations as vm',
                cache : false
            })
    };

    routes.$inject = ['$stateProvider','$urlRouterProvider'];

    angular
        .module('app.traininglocations')
        .config(routes);

}());
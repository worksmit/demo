(function(){

    'use strict';

    var traininglocations = function(datacontext,$scope,$parse,$uibModal,$uibModalStack){
    
        var vm = this;
        vm.head =  'Training Locations';
        vm.trainingLocationsForm = {};
        $scope.trainingLocationsForm = {};
        $scope.traininglocations = [];
        $scope.editIndex = {};
        $scope.selection = [];
        $scope.answers = [];
        $scope.totalPages = 0;
        $scope.currentPage = 1;
        $scope.range = [];
    

        $scope.getListing = function(pageNumber){

            $scope.traininglocations = [];
            if(pageNumber===undefined){
                pageNumber = '1';
            }

            datacontext.traininglocations.getAll(pageNumber).success(function(response){

                 if(response.status == 'success')
                    {
                        $scope.currentPage = response.traininglocations.current_page;
                        $scope.totalPages = response.traininglocations.last_page;
                        // Pagination Range
                        var pages = [];

                        for(var i=1;i<=response.traininglocations.last_page;i++) {          
                            pages.push(i);
                        }

                        $scope.range = pages; 

                        $scope.traininglocations = response.traininglocations.data;
                    }
            });
        }

        $scope.addTrainingLocation = function(){

            vm.trainingLocationsForm = {};
            
            $uibModal.open({
                templateUrl: 'addTrainingLocationModal.html',
                size: 'lg',
                controller: 'TrainingLocationModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }

        $scope.edit = function(index,size){

            $scope.trainingLocationsForm = angular.copy($scope.traininglocations[index]);
            $scope.editIndex = index;
            $uibModal.open({
                animation: true,
                templateUrl: 'editTrainingLocationModal.html',
                size: 'lg',
                controller: 'TrainingLocationModalInstanceCtrl',
                scope: $scope,
                backdrop: true
            })
            .result.then(function() {
                //alert('closed');
            }, function() {
                //alert('canceled');
            });
        }

        $scope.delete = function(index){

           

            if(confirm('Are you sure that you want to permanently delete this record?'))
            {
                datacontext.traininglocations.deleteTrainingLocation($scope.traininglocations[index].id).success(function(result){
                    if(result.status == 'success')
                    { 
                        $scope.traininglocations.splice(index,1);
                    }
                });
         
            }

        }


        $scope.stateChanged = function (index,id) {

            if($scope.answers[id]){ //If it is checked
               $scope.selection.push(id);
            }
            else
            {
                var idx = $scope.selection.indexOf(id);
                $scope.selection.splice(idx, 1);
            }
        }

        $scope.deleteSelected = function(){

            if(confirm('Are you sure that you want to permanently delete selected records?'))
            {
                datacontext.traininglocations.deleteSelected($scope.selection).success(function(result){
                    if(result.status == 'success')
                    { 
                        for (var j = $scope.selection.length-1; j >= 0 ; j--) {
                            angular.forEach($scope.traininglocations, function(value, key) {
                                if(value.id == $scope.selection[j])
                                {
                                    
                                    $scope.traininglocations.splice(key,1);
                                }
                            });
                            $scope.answers[$scope.selection[j]] = false;
                            $scope.stateChanged('',$scope.selection[j]);

                        }
                    }
                });
         
            }
        }
        
        
    };

    traininglocations.$inject = ['datacontext','$scope','$parse','$uibModal','$uibModalStack'];

    angular
        .module('app.traininglocations')
            .controller('traininglocations',traininglocations);

}());
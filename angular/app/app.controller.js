(function(){

    'use strict';

    var AppController = function($scope,$rootScope,$http,$state){

        
         if(!window.localStorage['token'])
         {
          $state.go('login');
         }
    
        $scope.logout = function(){

          window.localStorage.clear();
          $state.go('login');
        }
    };

    AppController.$inject = ['$scope','$rootScope','$http','$state'];

    angular
        .module('app')
            .controller('AppController',AppController);

}());